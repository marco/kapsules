# Kapsules - On-Premise Serverless -- Contribution Guidelines

Project   |  Kapsules
--------- | -----------------------
Version   |   0.6.1
Created   |   2019-04-14
Updated   |   2019-04-14
Author    |   M. Massenzio (marco@alertavert.com)

#### Copyright Kapsules.io (c) 2018. All rights reserved.

 [![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

---

# How to choose what to work on

We track the project's progress, and tasks/stories [on Pivotal tracker](https://www.pivotaltracker.com/n/projects/2149876): when contributing, it would be great if you could choose a task/story from the `Current Iteration backlog` or, if nothing tickles your fancy, from the `Icebox` - ideally in priority order.

If you have found a bug/feature that needs addressing, please add it first to the `Icebox`.

# How to be a good project citizen

We expect all contributions to follow the below:

- **Documentation** must be in Markdown syntax, following the GitHub syntax;
- **Code** must follow appropriate style guides ([Google for Java and Bash](https://github.com/google/styleguide), [PEP8](https://www.python.org/dev/peps/pep-0008/) for Python);
- **Kubernetes** all spec files **must** be in valid YAML, with a 2-space indent;
- **Tests must be added** and cover any code addition/changes.

Please do not push a Pull Request until all the code is properly formatted, and tests pass.


## Java Checkstyle

**TODO**
> Add automated checkstyle steps to the build scripts for Java.

## Python linter (`pylint`)

We use [PyLint](https://pylint.readthedocs.io) to validate code style compliance with PEP8; a customized configuration is in [`pylintrc`](python/pylintrc) and prevents certain violations to be reported that are nonetheless necessary (e.g., the `Event` protobuf class's members to be checked, as they are dynamically generated).

`pylint` check failures cause the commit to fail (run via the automated tests); if there is a **valid** reason that require a particular violation to be ignored, used the following pattern:

```python
        # Partition EOF is not really an error, it simply indicates
        # that there are no more messages pending in the topic,
        # after the timeout expired.
        if msg.error().code() != KafkaError._PARTITION_EOF:  # pylint: disable=W0212
```

In other words, make sure the violation is justified and **documented**.

# How to ensure your PR will be reviewed

Add the following `pre-commit` hook to your local repository:

    ln -s ../../scripts/pre-commit.sh .git/hooks/pre-commit

For local commits you can (temporarily! it's never a good idea to keep this on on a more-or-less permanent basis) define the `SKIPTESTS` variable:

```bash
export SKIPTESTS=1
git commit -m"My temerary commit"
```

Pushing a Pull Request that contains failing tests (or, almost as bad, untested code) will definitely **not** be reviewed or even considered.

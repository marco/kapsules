#!/usr/bin/env python3
#
# Build a serverless function container and publish it to the remote registry


import logging
from argparse import ArgumentParser
import os
from sh import docker

from pylambda.internals import ModuleBuilder
from pylambda.utils import (
    LogManager,
    create_namespace,
)

LOG = LogManager.get_logger(__file__)


def parse_args():
    parser = ArgumentParser(description="Builds a Serverless function from a Python "
                                        "source file; the function is associated with a "
                                        "Namespace and generates a Container suitable to "
                                        "be executed in the Serverless Project framework.")
    parser.add_argument('--ns', required=True,
                        help="Namespace for the generated function, formed by {domain, "
                             "org, scope} and separated by '::' (e.g., 'awesome.com::eng::local')")
    parser.add_argument('-f', dest='function', required=True,
                        help="Path to source file (must be a valid Python source file, ending in "
                             "`.py`)")
    parser.add_argument('-r', '--registry-url', default='registry:5000',
                        help="URL for the Docker registry for Serverless")
    parser.add_argument('-p', '--context', default=os.getenv('PYTHON_SRC', '.'),
                        help="Path to the Dockerfile context (default: uses "
                             "the $PYTHON_SRC env var)")
    parser.add_argument('-v', dest='verbose', action='store_true', help="Verbose logging")
    return parser.parse_args()


def main(opts):
    if opts.verbose:
        LOG.setLevel(logging.DEBUG)
    source = opts.function
    registry = opts.registry_url

    LOG.debug("Building image from source file '%s' and pushing to %s", source, registry)
    namespace = create_namespace(opts.ns)
    LOG.info("Building serverless function '%s' in namespace '%s'", source, namespace)
    builder = ModuleBuilder(source, registry_url=registry)
    builder.build_container(namespace)
    image = builder.image

    LOG.info("Built image: %s", image)
    docker.push(image)
    LOG.info("Published to Private registry: %s", registry)
    if not opts.verbose:
        print(image)

main(parse_args())

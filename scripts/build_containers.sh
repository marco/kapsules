#!/usr/bin/env bash
#
# Copyright Kapsules.io (c) 2018. All rights reserved.
#
# Builds Docker containers for all components of the Serverless project.


set -eu

source $(dirname $0)/env.sh
source $(dirname $0)/funcs.sh

declare -r JAVA_PROJECTS="gateway ingestion scheduler"
declare -r PYTHON_PROJECTS="base pylambda-builder"
declare -r REGISTRY="$(minikube -p serverless ip):${REGISTRY_PORT}"

if ! is_registry; then
    logerr "Registry is not reachable at ${REGISTRY}"
    exit 1
fi
logsuccess "Registry reachable at ${REGISTRY}"

### Minikube certs are regenerated every time the VM is recreated.
mkdir -p ${BASEDIR}/docker/certs && \
    cp ${HOME}/.minikube/{ca.crt,client.crt,client.key} ${BASEDIR}/docker/certs/

version=$(get_version)
if [[ -z ${version} ]]; then
    logerr "Could not determine Kapsules version for project"
    exit 1
fi
log "Building containers for version: ${version}"

### PYTHON Services
cd ${PYTHON_SRC}

# TODO: Need to activate the correct virtualenv?
for project in ${PYTHON_PROJECTS}; do
    image=${ORG}/${project}:${version}
    dockerfile="docker/Dockerfile.${project}"
    log "Building ${image} with Dockerfile: ${PYTHON_SRC}/${dockerfile}"

    docker build --build-arg version=${version} -t ${image} -f ${dockerfile} .
    docker tag ${image} ${REGISTRY}/${image}
    docker push ${REGISTRY}/${image}
    logsuccess "Image ${image} successfully built and pushed to ${REGISTRY}"
done

#### JAVA Services
cd ${BASEDIR}/kapsules


./gradlew -x test build
logsuccess "Built all Jars for Kapsules Rev. ${version}"

for project in ${JAVA_PROJECTS}; do
    cd ${BASEDIR}/kapsules/${project}

    jarfile="build/libs/${project}-${version}.jar"
    if [[ ! -e ${jarfile} ]]; then
        logerr "No JAR file available: $(pwd)/${jarfile} not found"
        exit 1
    fi

    tmpdir=/tmp/${project}_$$
    mkdir -p ${tmpdir}
    cp ${BASEDIR}/docker/Dockerfile.servers ${tmpdir}/Dockerfile
    cp -r $jarfile \
        ${BASEDIR}/docker/{entrypoint.sh,kube-config.yml,certs,lib64} \
        ${tmpdir}/

    image=${ORG}/${project}:${version}
    docker build --build-arg jarfile=$(basename $jarfile) \
        --build-arg project=${project} \
        -t ${image} ${tmpdir}

    docker tag ${image} ${REGISTRY}/${image}
    docker push ${REGISTRY}/${image}
    logsuccess "Image ${image} successfully built and pushed to ${REGISTRY}"

    rm -rf ${tmpdir}
done

logsuccess "All Java server containers built & pushed"

#!/bin/bash
#
# Copyright Kapsules.io (c) 2018. All rights reserved.

# Duplicates the code in funcs.sh - I do not wish env.sh to depend on that one.
function abspath {
    echo $(python -c "import os; print(os.path.abspath(\"$1\"))")
}


# See: https://stackoverflow.com/questions/59895/getting-the-source-directory-of-a-bash-script-from-within
export BASEDIR=$(abspath $(dirname "${BASH_SOURCE[0]}")/..)

export SCRIPTS="${BASEDIR}/scripts"
export PYTHON_SRC="${BASEDIR}/python"
export JAVA_SERVERS_DIR="${BASEDIR}/kapsules"
export PROTO_DIR="${BASEDIR}/proto"
export KUBE_TEMPLATES="${BASEDIR}/kubernetes"
export KUBE_DATA_TEMPLATES="${BASEDIR}/kubernetes/stateful"

# Protocol Buffer generation & sources.
export PROTO_SRC="${PROTO_DIR}/src/main/proto"
export PYTHON_OUT="${PROTO_DIR}/generated/main/python"
export JAVA_OUT="${PROTO_DIR}/generated/main/java"

export PYTHON_PROTOS=${PYTHON_SRC}/protos
export PYTHONPATH=".:${PYTHON_SRC}:${PYTHON_PROTOS}"

export OS="$(uname)"
export ORG='kapsules'

export REGISTRY_PORT=30500

export MINIKUBE_RAM_MB=8192
export MINIKUBE_CPUS=4

#!/bin/bash
#
# Copyright Kapsules.io (c) 2018. All rights reserved.


# Prints the absolute path of the file given as $1
#
function abspath {
    echo $(python -c "import os; print(os.path.abspath(\"$1\"))")
}

function get_version {
    cd ${PYTHON_SRC}
    version=$(pipenv run pylambda/version.py -g)
    echo ${version}
}

function timestamp {
    echo $(date "+%Y-%m-%d %H:%M:%S")
}


# Logs a message to stdout, using the given LEVEL
#
# Args:
#   LEVEL   one of INFO, SUCCCESS or ERROR
#   MSG ... sequence of log messages, will be all emittted on one line
#
function logbase {
    local level=${1:-}
    shift 1

    echo "$(timestamp) [${level}] $@"
}


function log {
    logbase INFO $@
}

function logerr {
    logbase ERROR $@
}

function logsuccess {
    logbase SUCCCESS $@
}


# Attempts to connect to the Docker Registry
#
function is_registry {
    if [[ -z ${REGISTRY} ]]; then
        echo "[ERROR] \$REGISTRY is not defined"
        return 1
    fi

    echo "Connecting to registry at ${REGISTRY}..."
    if curl --head -fs http://${REGISTRY}/v2/ -o /dev/null; then
        return 0
    fi
    return 1
}

# Checks whether the Minikube cluster is up & running
#
function is_minikube_up {
    if [[ -z $(minikube status | grep "cluster: Running") ]]; then
        return 1
    fi
    return 0
}

# Starts up the Minikube cluster
#
# Args:
#  RAM      amount of memory for the VM in MB
#  CPUS     number of vCPUs for the VM
#
function start_minikube {
    local ram=${1:-8192}
    local cpus=${2:-4}

    if is_minikube_up; then
        logerr "Minikube is already running"
        minikube status
        return 1
    fi

    minikube start --memory ${ram} --cpus ${cpus} \
        --insecure-registry registry

    if [[ $? != 0 ]]; then
        logerr "Could not start Minikube, cannot deploy Kapsules"
        return 1
    fi
    logsuccess "Minikube started"
    minikube ssh -- "echo -e '\n127.0.0.1\tregistry\n' \
        | sudo tee -a /etc/hosts >/dev/null"
    log "Registry entry added to /etc/hosts in Minikube VM"

    minikube status
}

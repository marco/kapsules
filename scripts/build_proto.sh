#!/bin/bash
#
# Copyright Kapsules.io (c) 2018. All rights reserved.
#
# Builds generated sources from Protobuf.

set -eu

source $(dirname $0)/funcs.sh
source $(dirname $0)/env.sh

mkdir -p ${PYTHON_PROTOS}

cd ${PROTO_DIR}

version=$(cat gradle.properties | grep version | cut -f 2 -d '=')
version=${version//[[:space:]]/}

log "Building Kapsules Protobuf ver. ${version}"
./gradlew clean install
cp ${PYTHON_OUT}/*.py ${PYTHON_PROTOS}/

jar=$(basename $(ls build/libs/*.jar | grep ${version}))

logsuccess "Proto JAR Rel. ${version} (${jar}) installed to local Maven Repository"
log "Python sources copied to ${PYTHON_PROTOS}"

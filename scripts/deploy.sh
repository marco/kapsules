#!/bin/bash
#
# Copyright Kapsules.io (c) 2018. All rights reserved.
#

set -eu

source $(dirname $0)/funcs.sh
source $(dirname $0)/env.sh


if ! is_minikube_up; then
    logerr "Minikube cluster is not running"
    if ! start_minikube ${MINIKUBE_RAM_MB} ${MINIKUBE_CPUS}; then
        logerr "Cannot start Minikube cluster"
        exit 1
    fi
fi
log "Kubernetes cluster (Minikube) is available at $(minikube ip)"

REGISTRY="$(minikube ip):${REGISTRY_PORT}"
if ! is_registry; then
    logerr "Registry is not reachable at ${REGISTRY}, trying to deploy Pod"
    kubectl apply -f ${KUBE_TEMPLATES}/registry.yaml

    # Wait a beat for the registry to come up
    retries=15
    log "Waiting for registry to come up"
    while [[ -z ${is_registry_ready:-} && ${retries} > 0 ]]; do
        is_registry_ready=$(kubectl get pod \
            --field-selector metadata.name=registry,status.phase=Running \
            --no-headers 2>/dev/null | cut -f 1 -d ' ')
        retries=$((${retries} - 1))
        sleep 1
        echo -n " . ${retries}"
    done
    echo " done"
    if ! is_registry; then
        logerr "Registry Pod is not running yet, wait for it come online and retry deployment"
        exit 1
    fi
fi
log "Registry reachable at ${REGISTRY}"

${SCRIPTS}/build_containers.sh

CONFIGS=($(find ${KUBE_TEMPLATES} -name "*.yaml"))
for yaml in ${CONFIGS[*]}; do
    log "Deploying ${yaml}"
    kubectl apply -f ${yaml}
done

logsuccess "All Pods deployed, use 'kubectl get po -w' to monitor progress"
kubectl get po

#!/bin/bash
#
# Pre-commit hook, autoformats the code & run tests.

set -ex

cd $(dirname $0)/../..
if [[ -z ${SKIPTESTS} ]]; then
	./scripts/run_tests
fi

cd python
pipenv run black -l 100 pylambda tests

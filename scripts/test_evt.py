#!/usr/bin/env python

from confluent_kafka import Producer
from serverless_pb2 import Event, Source, Namespace

ns = Namespace()
ns.org = 'org'
ns.scope = 'scope'

s = Source()
s.namespace.CopyFrom(ns)
s.name = 'test'

evt = Event()
evt.source.CopyFrom(s)
evt.namespace.CopyFrom(ns)
evt.eventType = 'earthquake'

p = Producer({'bootstrap.servers': 'localhost:9092'})
p.produce('serverless.topics.activations', evt.SerializeToString())

print(f"Sent EVENT to Kafka:\n{evt}")

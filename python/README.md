# Build Server

Project   |  On-Premise Serverless
--------- | -----------------------
Component |   Build Server
Version   |   0.1.0
Created   |   2018-01-13
Updated   |   2018-01-14
Author    |   M. Massenzio (marco@alertavert.com)

---

# Scope

Given a [Serverless Function](#serverless_function_api) this builds a Docker container image that will execute the function and will upload it to an internal, private Docker registry.


`Serverless Function`
> A Python "module" written according to the [Serverless API](#Api) guidelines, and containing, at the very least, an implementation of a class derived from the `LambdaBase` class.

# Build


```shell

    cd pylambda
    docker build -t localhost:5000/serverless/base:0.1.0 .
    docker push localhost:5000/serverless/base:0.1.0
```

# Usage


## Serverless Function API

# Examples

Running the Build server:

    from pylambda.internals import BuildServer
    import asyncio

    loop = asyncio.get_event_loop()
    server = BuildServer(verbose=True)
    loop.run_until_complete(server.start())
    # This will run forever until interrupted by Ctrl-C
    # Add a signal handler to terminate gracefully

Sending an event on the `activations` topic:

    from confluent_kafka import Producer
    from serverless_pb2 import Event, Source, Namespace

    ns = Namespace()
    ns.org = 'org'
    ns.scope = 'scope'

    s = Source()
    s.namespace.CopyFrom(ns)

    evt = Event()
    evt.source.CopyFrom(s)
    evt.namespace.CopyFrom(ns)
    evt.eventType = 'earthquake'

    p = Producer({'bootstrap.servers': 'kafka:9092'})
    p.produce('serverless.topics.activations', evt.SerializeToString())


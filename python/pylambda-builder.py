#!/usr/bin/env python
#
# @copyright: Kapsules.io (c) 2018. All rights reserved.
#
import argparse
import logging
from pylambda.internals import BuildServer
import asyncio


from pylambda.utils import LogManager


def parse_args():
    """ Parse command line arguments and returns a configuration object.

    @return: the configuration object, arguments accessed via dotted notation
    @rtype: dict
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--port', type=int, default=5050,
                        help="Port the server will be listening on")
    parser.add_argument('--kafka', required=True,
                        help="A comma-separated list of Kafka brokers URIs, in the `host:port` "
                             "format")
    parser.add_argument('--redis', required=True,
                        help="A comma-separated list of Redis servers URIs, in the `host:port` "
                             "format")
    parser.add_argument('--registry-uri', default='registry:5000',
                        help="The URI for the private Docker Registry")
    parser.add_argument('--debug', '-v', default=False, action='store_true')
    return parser.parse_args()


def main(cfg):
    """ Runs a Build Server, listening to incoming Kafka events and creates Serverless Functions.

    :param cfg: CLI args
    :type cfg: argparse.Namespace
    """
    log = LogManager.get_logger(__name__, logging.DEBUG if cfg.debug else logging.INFO)
    log.info(f"Starting Serverless Build Server on port {cfg.port}")
    log.info(f"  - Kafka brokers: {cfg.kafka}")
    log.info(f"  - Redis cluster: {cfg.redis}")
    log.info(f"  - Docker Registry URI: {cfg.registry_uri}")
    log.info("Terminate the server with Ctrl-C")

    loop = asyncio.get_event_loop()
    server = BuildServer(kafka_brokers=cfg.kafka.split(','),
                         port=cfg.port,
                         registry=cfg.registry_uri,
                         redis=cfg.redis,
                         verbose=cfg.debug)

    try:
        loop.run_until_complete(server.start())
    except KeyboardInterrupt:
        log.info("Shutting down server")
        loop.run_until_complete(server.stop())
        loop.stop()
        loop.close()
        log.info("Done, exiting")


if __name__ == '__main__':
    options = parse_args()
    main(options)

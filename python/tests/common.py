# Copyright Kapsules.io (c) 2018. All rights reserved.


from pathlib import Path
import unittest


class TestBase(unittest.TestCase):
    def setUp(self):
        self.tests_dir = Path(__file__).parent
        self.data_dir = self.tests_dir / "data"

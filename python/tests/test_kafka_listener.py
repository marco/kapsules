# Copyright Kapsules.io (c) 2018. All rights reserved.

import asyncio

from forbiddenfruit import curse, reverse
from unittest.mock import MagicMock

from confluent_kafka import Consumer

import common
from pylambda.kafka import KafkaListener

count = 0


class TestKafkaListener(common.TestBase):
    def setUp(self):
        super().setUp()

    @classmethod
    def tearDownClass(cls):
        loop = asyncio.get_event_loop()
        if not loop.is_closed():
            while loop.is_running():
                yield from asyncio.sleep(0.5)
            loop.close()

    def test_one_msg(self):
        def mock_poll(self, timeout=None):
            message = MagicMock()
            message.value = MagicMock(return_value="this is a test".encode("utf-8"))
            message.error = MagicMock(return_value=None)
            return message

        listener = KafkaListener("test-topic", "test-group", bootstrap_servers="localhost:9099")
        curse(Consumer, "poll", mock_poll)

        @asyncio.coroutine
        def read_one_msg(self, timeout=None):
            msg = yield from listener.messages.get()
            self.assertEqual("this is a test", msg.decode("utf-8"))
            yield from listener.stop()

        loop = asyncio.get_event_loop()
        wait_coro = asyncio.wait([read_one_msg(self), listener.run_consumer()])
        res, _ = loop.run_until_complete(wait_coro)
        # Free the listener from its curse, to avoid impacting other tests
        reverse(Consumer, "poll")

    def test_many_msgs(self):
        messages = [f"Message #{i}".encode("utf-8") for i in range(1, 10)]

        def mock_poll(self, timeout=None):
            global count
            message = MagicMock()
            message.value.return_value = messages[count]
            count += 1
            message.error = MagicMock(return_value=None)
            return message

        listener = KafkaListener("another-topic", "group2", bootstrap_servers="localhost:9093")
        curse(Consumer, "poll", mock_poll)

        @asyncio.coroutine
        def read_msgs(self):
            for i in range(1, 4):
                msg = yield from listener.messages.get()
                self.assertEqual(f"Message #{i}", msg.decode("utf-8"), f"At step {i}")
            yield from listener.stop()

        def supervisor(self):
            coro_iter = asyncio.as_completed([read_msgs(self), listener.run_consumer()])
            for result in coro_iter:
                try:
                    res = yield from result
                except Exception as ex:
                    self.fail(ex)

        loop = asyncio.get_event_loop()
        loop.run_until_complete(supervisor(self))
        reverse(Consumer, "poll")

# Copyright Kapsules.io (c) 2018. All rights reserved.
import os

import common
from pylambda.utils import create_namespace


class TestLambda(common.TestBase):
    def setUp(self):
        super().setUp()

    def test_create_namespace(self):
        namespace = create_namespace("foo::bar::qoz")
        self.assertEqual("foo", namespace.domain)
        self.assertEqual("bar", namespace.org)
        self.assertEqual("qoz", namespace.scope)

    def test_create_namespace_with_sep(self):
        namespace = create_namespace("foo/bar/qoz", separator="/")
        self.assertEqual("foo", namespace.domain)
        self.assertEqual("bar", namespace.org)
        self.assertEqual("qoz", namespace.scope)

    def test_create_ns(self):
        ns = create_namespace("test.com::eng::local")
        self.assertIsNotNone(ns)
        self.assertEqual("local", ns.scope)

        with self.assertRaises(ValueError):
            create_namespace("test.com,eng: local")

        ns = create_namespace("awesome.com-marketing-emea", separator="-")
        self.assertIsNotNone(ns)
        self.assertEqual("emea", ns.scope)
        self.assertEqual("marketing", ns.org)

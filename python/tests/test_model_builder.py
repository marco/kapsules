# Copyright Kapsules.io (c) 2018. All rights reserved.
import os
from unittest.mock import patch, ANY

import common
from pylambda.internals import ModuleBuilder


class TestLambda(common.TestBase):
    def setUp(self):
        super().setUp()
        self.fname = self.data_dir / "entry.py"
        self.builder = ModuleBuilder(self.fname)

    def test_build_entrypoint(self):
        result = self.builder._build_entrypoint()
        self.assertIn("from entry import SampleLambda", result)
        self.assertIn("instance = SampleLambda(event, log_level=level)", result)

    def test_invalid_fails(self):
        flawed_builder = ModuleBuilder(self.data_dir / "invalid_lambda.py")
        with self.assertRaises(ValueError):
            flawed_builder._build_entrypoint()

    def test_build_dockerfile(self):
        result = self.builder._build_dockerfile()
        self.assertIn("ENTRYPOINT python entrypoint.py", result)
        self.assertIn("ADD entrypoint.py entry.py ./", result)

    @patch("pylambda.internals.cp")
    @patch("pylambda.internals.docker")
    def test_build_container(self, docker_mock, cp_mock):
        self.builder.build("test-func", "1.0.0")
        docker_img = self.builder.image
        self.assertEqual("localhost:5000/test-func:1.0.0", docker_img)
        cp_mock.assert_called_once_with(os.path.abspath(self.fname), ANY)
        docker_mock.assert_called_once_with("build", "-t", docker_img, ANY)

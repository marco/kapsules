import json
import unittest
import serverless_pb2 as protos

import pylambda.utils

from google.protobuf.json_format import MessageToJson


class TestProtos(unittest.TestCase):
    def test_can_get_Event(self):
        ns = protos.Namespace()
        ns.domain = "test.domain"
        ns.org = "test-org"
        ns.scope = "test"

        src = protos.Source()
        src.namespace.CopyFrom(ns)
        src.name = "test source"

        event = protos.Event()
        event.source.CopyFrom(src)
        event.namespace.CopyFrom(src.namespace)
        event.eventType = "test-event"

        # Serialize the event to JSON, then get it back and compare.
        json_ser = MessageToJson(event)

        evt_as_dict = json.loads(json_ser)
        self.assertEqual("test.domain", evt_as_dict["source"]["namespace"]["domain"])
        self.assertEqual("test-event", evt_as_dict["eventType"])

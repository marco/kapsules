# This is not a valid serverless file, used for tests only.

from common import TestBase


def foo(ter):
    print("not really")


class FakeLambda(TestBase):
    pass


if __name__ == "__main__":
    lambda_t = FakeLambda()
    foo(lambda_t)

from pylambda.base import LambdaBase


class SampleLambda(LambdaBase):
    def run(self):
        self.log.info("This is being executed in the lambda")

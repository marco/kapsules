#!/bin/bash

CMD="./pylambda-builder.py --port=${PORT} --kafka=${KAFKA_BOOTSTRAP_URL} --redis=${REDIS_URL} "\
"--registry=${REGISTRY_URI} ${DEBUG}"

echo "${CMD}"
eval ${CMD}

# Copyright Kapsules.io (c) 2018. All rights reserved.

"""Utility methods, and classes"""

import logging

from serverless_pb2 import Namespace


class LogManager:
    """Utility class to centralize logging configuration and management"""

    _enable_debug = False
    _default_level = logging.INFO

    @classmethod
    def enable_debug_info(cls):
        """ Enable debug information in log output.

        After invocation, it will add the module name and line number of the
        logging event; however this impacts performance, so best only enabled for debugging
        purposes.

        It cannot be reset after invocation.
        """
        cls._enable_debug = True

    @classmethod
    def set_default_level(cls, level):
        """ The default level for any logger created without one explicitly requested"""
        cls._default_level = level

    @classmethod
    def get_logger(cls, logname, level=None):
        """ Gets the named logger, optionally setting the level and additional debug info.

        :param logname: the name of the desired logger
        :param level: the logging level, by default `logging.WARN` (unless changed via the
        `set_default_level()` class method.

        :return: the named logger
        :rtype: logging.Logger
        """
        logger = logging.getLogger(logname)
        logger.setLevel(level or cls._default_level)

        fmt = (
            "%(asctime)s %(module)s:%(lineno)d [%(levelname)-6s] %(message)s"
            if cls._enable_debug
            else "%(asctime)s [%(levelname)-6s] %(message)s"
        )

        formatter = logging.Formatter(fmt=fmt, datefmt="%Y-%m-%d %H:%M:%S")

        handler = logging.StreamHandler()
        handler.setFormatter(formatter)
        logger.addHandler(handler)
        return logger


def create_namespace(namespace, separator="::"):
    """ Creates a `Namespace` protobuf from a string representation of its components

    :param namespace: a concatenation of the namespace components (domain, org, scope) separated
    by `separator`
    :type namespace: str

    :param separator: the separator between components (by default '::')
    :type separator: str

    :return: the corresponding `Namespace` object
    :type: Namespace
    """
    nspace = Namespace()
    nspace.domain, nspace.org, nspace.scope = namespace.split(separator, maxsplit=3)
    return nspace

# Copyright Kapsules.io (c) 2018. All rights reserved.


"""Defines a set of commonly-used constants, related to useful paths."""

import pathlib


# BASEDIR = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), ".."))
BASEDIR = pathlib.Path(__file__).parent.parent.absolute()
TEMPLATES = BASEDIR / "templates"
PROTO_SRCS = BASEDIR / "generated"

DOCKERFILE_TEMPLATE = "Dockerfile.template"
ENTRYPOINT_TEMPLATE = "entrypoint.py.template"

DOCKERFILE_TEMPLATE_PATH = TEMPLATES / DOCKERFILE_TEMPLATE
ENTRYPOINT_TEMPLATE_PATH = TEMPLATES / ENTRYPOINT_TEMPLATE

KAPSULES_VERSION_PROPERTY = "KAPSULES_VERSION"

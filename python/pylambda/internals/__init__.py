# Copyright Kapsules.io (c) 2018. All rights reserved.

"""
Internal classes and methods to build Kapsules; these will NOT be available to the lambda
functions, but are only used internally to enable functionality.

This module includes two classes, a `BuildServer` that serves the APIs to receive incoming
source code and build the Kapsule (container) and a `ModuleBuilder` that does the actual
building of the code and the resulting container.
"""

import asyncio
import json
import logging
import os
import re
import sys
import traceback
import uuid
from tempfile import TemporaryDirectory, NamedTemporaryFile

import jinja2
from aiohttp import web
from google.protobuf.message import DecodeError
from sh import cp, docker, ErrorReturnCode

import serverless_pb2 as protos
from pylambda import TEMPLATES, ENTRYPOINT_TEMPLATE, DOCKERFILE_TEMPLATE
from pylambda.kafka import KafkaListener
from pylambda.utils import LogManager
from pylambda.version import get_version

ENTRYPOINT = "entrypoint.py"

LAMBDA_CLASS_PATTERN = re.compile(r"class\s+(?P<classname>\S+)\(LambdaBase\):")


class ModuleBuilder:
    """ A ModuleBuilder creates a Docker image from a serverless function

        :param fname: the name of the Python file containing the source code for the function;
        should be the full path to the `.py` file, whose basename will be extracted and used as
        the function's image name
        :type fname: Path

        :param registry_url: the URL for the local registry to publish the image to
        :type registry_url: URL

        :param verbose: if set to `True` it will emit logs at the DEBUG level
        :type verbose: bool
        """

    def __init__(self, fname, registry_url="localhost:5000", verbose=False):
        self._log = LogManager.get_logger(self.__class__.__name__)
        if verbose:
            self._log.setLevel(logging.DEBUG)

        if not fname.match("*.py"):
            raise ValueError("The entrypoint must be a Python source file")

        self.source = fname.absolute()
        self.module_basename = fname.name[:-3]
        self.jinja_env = jinja2.Environment(loader=jinja2.FileSystemLoader(str(TEMPLATES)))
        self._image = None
        self.repository_url = registry_url

    @property
    def image(self):
        """The name of the container Image. """
        return self._image

    def _build_entrypoint(self):
        self._log.info("Rendering template (%s) for serverless module: %s", ENTRYPOINT, self.source)
        template = self.jinja_env.get_template(ENTRYPOINT_TEMPLATE)

        class_name = self._extract_class_name()
        if not class_name:
            raise ValueError("The entrypoint module must define a class derived from LambdaBase")
        render_py = template.render(clazz=class_name, module=self.module_basename)
        self._log.debug("Python Entrypoint generated: %s", render_py)
        return render_py

    def _build_dockerfile(self):
        self._log.info("Generating Dockerfile for serverless module: %s", self.source)
        template = self.jinja_env.get_template(DOCKERFILE_TEMPLATE)
        render_dockerfile = template.render(
            base_version=get_version(),
            entrypoint=ENTRYPOINT,
            registry_url=self.repository_url,
            lambda_module="{}.py".format(self.module_basename),
        )
        self._log.debug("Dockerfile generated: %s", render_dockerfile)
        return render_dockerfile

    def build(self, name, version=None):
        """ Builds the actual Container image and publishes to the Registry.

        :param name: the name of the `function` to be deployed; will be part of the Docker image tag
        :type name: str

        :param version: an optional image version (as in `latest` or `0.1.1`) to be used as part
        of the image `tag` (`registry/org/name:version`). Optional; if unspecified a 6-digit
        random hex string will be used instead
        :type version: str

        """
        version = version or str(uuid.uuid4())[:6]

        with TemporaryDirectory() as tmp_dir:
            with open(os.path.join(tmp_dir, ENTRYPOINT), "wt") as entrypoint:
                entrypoint.write(self._build_entrypoint())
            with open(os.path.join(tmp_dir, "Dockerfile"), "wt") as dockerfile:
                dockerfile.write(self._build_dockerfile())

            cp(str(self.source), tmp_dir)
            tag = f"{self.repository_url}/{name}:{version}"

            self._log.info("Building Container for Module: %s (%s)", self.source, tag)
            try:
                docker("build", "-t", tag, tmp_dir)
                self._image = tag
                self._log.info("Container image built: %s", self._image)
            except ErrorReturnCode as err:
                self._log.error("Could not build image (%s): %s", err.exit_code, err.stderr)
                raise RuntimeError(f"Failed to build {name} as {tag}: {err.stderr}")

    def _extract_class_name(self):
        """ Extracts the name of the class from the source code, for replacement in the template.

        :return: the name of the entry point class for the Kapsule, which must extend `LambdaBase`
        :rtype: str
        """
        with self.source.open() as src:
            for line in src.readlines():
                match = re.search(LAMBDA_CLASS_PATTERN, line)
                if match:
                    return match.group("classname")
        return None


class BuildServer:
    """ A Build server waits for incoming Kafka messages and build serverless containers.

    It also exposes a minimal API and logs messages and other events to a Kafka topic.

    :param port: the port the server is listening on for incoming API requests
    :type port: int

    :param kafka_brokers: list of Kafka brokers ('host:port' format)
    :type kafka_brokers: list

    :param registry: the URI for the private Docker registy
    :type registry: str

    :param redis: an optional URL for the Redis cluster.
    :type redis: str

    :param loop: the event loop to use for the Web server and the Kafka listener
    :type loop: AbstractBaseLoop

    :param verbose: if set to `True` it will log at DEBUG level
    :type verbose: bool
    """

    # fmt: off
    def __init__(self, port=5002, kafka_brokers=None, registry="registry:5000",
                 redis="redis:6379", loop=None, verbose=False):
        self._log = LogManager.get_logger(self.__class__.__name__)
        self.verbose = verbose
        if self.verbose:
            self._log.setLevel(logging.DEBUG)
        self.loop = loop or asyncio.get_event_loop()
        self.port = port
        self.redis_url = redis
        self.registry_url = registry
        # TODO: use configuration values read from a YAML file.
        self.kafka_listener = KafkaListener(
            "serverless.build",
            "build-servers",
            bootstrap_servers=",".join(kafka_brokers or ["localhost:9092"]),
            debug=self.verbose,
        )
        self.api_server = None
        self.web_app = web.Application(loop=asyncio.get_event_loop())
        self._create_routes()
    # fmt: on

    def _create_routes(self):
        self.web_app.router.add_route("GET", "/", self.home)
        self.web_app.router.add_route("GET", "/stop", self.stop_server)

    async def stop(self):
        """Stops the server, terminating the Kafka consumer too."""
        await self.kafka_listener.stop()
        self._log.debug("Kafka Listener stopped consuming messages")
        self.api_server.close()
        await self.api_server.wait_closed()
        self._log.debug("Async Server stopped & closed")

    async def _start_server(self):
        handler = self.web_app.make_handler()
        self.api_server = await self.loop.create_server(handler, "0.0.0.0", self.port)
        return self.api_server.sockets[0].getsockname()

    async def _start_listener(self):
        while not self.kafka_listener.stopped:
            try:
                msg = self.kafka_listener.messages.get_nowait()
            except asyncio.queues.QueueEmpty:
                await asyncio.sleep(3)
                continue
            self._log.debug("Queue size: %s", self.kafka_listener.messages.qsize())
            event = protos.Event()
            try:
                event.ParseFromString(msg)
                self._log.debug("Building Serverless Function from Event: %s", event.source)
                # TODO: this is blocking, must be placed in a coroutine
                self.build_serverless(event)

            except (DecodeError, RuntimeError) as ex:
                self._log.error("Cannot build serverless function from event: %s", ex)
                if self.verbose:
                    _, _, trace = sys.exc_info()
                    traceback.print_tb(trace)

        self._log.info("KafkaListener stopped")

    def build_serverless(self, event):
        """ Builds a Kapsule, as triggered by the Event.

        :param event: a protobuf that describes how the Kapsule should be built, and its contents.
        :type event: Event
        """
        self._log.debug("Received a BUILD event: %s", event)
        if event.eventType != "build":
            self._log.warning("Expected a `build` eventType, got instead: %s", event.eventType)

        source = event.source
        name, version = source.name.split(":")
        if not (name and version):
            self._log.warning(
                "Expected Source.Name to carry the `name:version` for the "
                "Serverless function, found instead: '%s'",
                source.name,
            )
        with NamedTemporaryFile(suffix=".py") as tmp_src:
            # TODO: use payloadUri, if set.
            if not event.serializedAs == protos.Event.TEXT:
                raise ValueError(
                    "A function MUST be serialized as `TEXT`; found instead: "
                    f"`{protos.Event.SerializedAs.Name(event.serializedAs)}`"
                )
            tmp_src.write(event.payload)
            tmp_src.flush()
            builder = ModuleBuilder(tmp_src.name, self.registry_url, self.verbose)
            builder.build(name, version)
            self._log.info("Built image `%s` from event [%s]", builder.image, event.id)
            try:
                # TODO: publish an Event to Kafka topic
                docker.push(builder.image)
                self._log.info(
                    "Image `%s` pushed to remote Registry [%s]", builder.image, self.registry_url
                )
            except ErrorReturnCode as err:
                self._log.error("Could not publish image `%s`: %s", builder.image, err.stderr)
                raise RuntimeError(f"Failed to build {name} as {builder.image}: {err.stderr}")

    async def start(self):
        """Starts the server, entry point for the application."""
        coro_iter = asyncio.as_completed(
            [
                self.kafka_listener.run_consumer(self.loop),
                self._start_listener(),
                self._start_server(),
            ]
        )
        try:
            for coro in coro_iter:
                await coro
        except Exception as ex:
            self._log.error(ex)
            raise

    @property
    def configs(self):
        """Returns all the server configurations"""
        return {
            "server_port": self.port,
            "kafka.bootstrap_urls": self.kafka_listener.brokers,
            "kafka.topic": self.kafka_listener.topic,
            "kafka.group_id": self.kafka_listener.group_id,
            "redis.url": self.redis_url,
            "registry.url": self.registry_url,
        }

    # MARK: Routes
    def home(self, _):
        """Default route"""
        return web.Response(content_type="application/json", text=json.dumps(self.configs))

    async def stop_server(self, _):
        """Stops the server"""
        await self.stop()
        self._log.debug("Server closed")
        return web.Response(content_type="text/plain", body="Server stopped")

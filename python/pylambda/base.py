# Copyright Kapsules.io (c) 2018. All rights reserved.

"""Defines the base classes for all Lambda functions."""

import logging

import collections
from redis import Redis

import serverless_pb2 as protos
from pylambda.utils import LogManager

Source = collections.namedtuple("Source", ["domain", "system", "stream"])


class LambdaBase:
    """ Base class for all lambdas, callable.

        :param event: the `Event` that triggered execution for this lambda.
        :type event: protos.Event

        :param log_level: the logging level for the function, INFO by default
        :type log_level: int
    """

    def __init__(self, event, log_level=logging.INFO):
        self.event = event
        self.log = LogManager.get_logger(self.__class__.__name__, log_level)
        self.log.debug("Created Kapsule for event [%s]", self.event.id)

    def run(self):
        """ Execution entrypoint, must be overridden in the lambda implementation """
        raise NotImplementedError()

    def __call__(self):
        """ Invoked to start execution of the lambda, takes no arguments"""
        self.log.info("Starting Kapsule, triggered by event ID: %s", self.event.id)
        self.run()


def build_event(event_id, redis_url):
    """ Builds an `Event` object for the Kapsule

    :param event_id: the UUID of the event, stored in Redis
    :type event_id: str

    :param redis_url: the Redis server URL, in `host:port` format
    :type redis_url: str

    :return: the event that triggered this Kapsule, if available
    :rtype: protos.Event
    """
    if not event_id:
        raise EnvironmentError("EVENT_ID not defined, cannot retrieve triggering event")

    host, port = redis_url.split(":")
    redis = Redis(host=host, port=port)
    raw_data = redis.get(event_id)
    if raw_data:
        return protos.Event.FromString(raw_data)
    raise RuntimeError(f"Could not find an event with ID [{event_id}]")

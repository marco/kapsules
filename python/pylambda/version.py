#!/usr/bin/env python
# Copyright Kapsules.io (c) 2018. All rights reserved.

"""Utility scripts to extract version values for various builds."""

import os
import re
import  sys

from pathlib import Path

from pylambda import KAPSULES_VERSION_PROPERTY

VERSION_PATTERN = r"version\s*=\s*(?P<version>\S+)"


def get_version():
    """ Retrieves the current version for the system.

        Tries to use the ```KAPSULES_VERSION_PROPERTY``` environment variable; if not
        defined, it will assume it is a development environment and tries and
        derive it from the ```gradle.properties``` for the Java Servers.
    """
    _version = os.getenv(KAPSULES_VERSION_PROPERTY)
    if not _version:
        properties = Path(os.getenv("BASEDIR") or Path.cwd()) / "kapsules" / "gradle.properties"
        if not properties.exists():
            raise FileNotFoundError(
                f"Gradle properties file {properties} does not exist; "
                f"have you defined the $BASEDIR env var? (current "
                f"value: '{os.getenv('BASEDIR', '')}')"
            )
        _version = get_gradle_version(properties)
    return _version


def get_gradle_version(gradle_properties):
    """Retrieves the version property from the Gradle configuration file"""
    with open(gradle_properties) as properties:
        for line in properties.readlines():
            match = re.search(VERSION_PATTERN, line)
            if match:
                return match.group("version")
    raise ValueError("No version line found")


if __name__ == "__main__":
    try:
        print(get_version())
    except ValueError as ex:
        print(f"[ERROR] No version available: {ex}")
        sys.exit(1)
    except FileNotFoundError as ex:
        print(f"[ERROR] No properties file: {ex}")
        sys.exit(1)

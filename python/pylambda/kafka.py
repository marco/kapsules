# Copyright Kapsules.io (c) 2018. All rights reserved.

"""An asyncio Kafka listener class, non-blocking Consumer client. """

import asyncio
from asyncio import Queue
import logging

from confluent_kafka import Consumer, KafkaError

from pylambda.utils import LogManager
import serverless_pb2 as protos

__author__ = "Marco Massenzio"
__email__ = "marco@alertavert.com"


class KafkaListener:
    """ An async consumer client for Kafka.

        :param topic: the Kafka Topic this consumer will subscribe to
        :param group_id: used to implement the Pub/Sub or Queue model; see Kafka documentation
        for the use of `group_id` for a set of clients subscribing to the same topic.

        :param bootstrap_servers: optional `host:port` comma-separated host URLs for the
        Kafka brokers; these may not necessarily be the actual Brokers, but will implement the
        Kafka protocol to redirect the client to the brokers.
        :type bootstrap_servers: str

        :param kwargs: additional configurations for the Consumer (currently not used)
        """

    CONSUMER_QUEUE_MAXSIZE = 1000
    CONSUMER_POLL_TIMEOUT_DEFAULT = 5

    def __init__(self, topic, group_id, bootstrap_servers="localhost:9092", **kwargs):
        level = logging.DEBUG if kwargs.get("debug") else logging.INFO
        self._log = LogManager.get_logger(self.__class__.__name__, level=level)
        self.consumer_poll_timeout = kwargs.pop(
            "consumer_poll_timeout", KafkaListener.CONSUMER_POLL_TIMEOUT_DEFAULT
        )
        self.messages = Queue(
            kwargs.pop("consumer_queue_maxsize", KafkaListener.CONSUMER_QUEUE_MAXSIZE)
        )
        self.brokers = bootstrap_servers
        consumer_args = {
            "bootstrap.servers": bootstrap_servers,
            "group.id": group_id,
            "default.topic.config": {"auto.offset.reset": "smallest"},
        }
        consumer_args.update(kwargs)
        self.consumer = Consumer(**consumer_args)
        self.topic = topic
        self.group_id = group_id
        self.consumer.subscribe([self.topic])
        self._log.debug("Connected to Kafka brokers '%s', for topic: %s", bootstrap_servers, topic)
        self.stopped = False
        self.consuming = None

    async def stop(self):
        """Stops the listener, unblocking it, if necessary."""
        self._log.debug("Stopping KafkaListener")
        self.stopped = True

        # Unblock the clients awaiting on the messages queue.
        fake_event = protos.Event()
        await self.messages.put(fake_event)

        while self.consuming:
            await asyncio.sleep(0.5)
        self._log.debug("KafkaListener stopped")

    def _poll(self):
        """ Wrapper around the `Consumer.poll()` blocking call"""
        msg = self.consumer.poll(timeout=self.consumer_poll_timeout)
        if msg is None:
            return None
        if not msg.error():
            return msg.value()

        # Partition EOF is not really an error, it simply indicates there are no more messages
        # pending in the topic, after the timeout expired.
        if msg.error().code() != KafkaError._PARTITION_EOF:  # pylint: disable=W0212
            # TODO: Should post an error message to a "dead letter" queue.
            self._log.error("Kafka error (%d): %s", msg.error().code(), msg.error())
        return None

    async def run_consumer(self, loop=None):
        """ Entry point for the listener.

        :param loop: an optional asyncio event loop; if not provided, the default event loop will
        be used
        :type loop: AbstractEventLoop
        :return: None
        """
        loop = loop or asyncio.get_event_loop()
        self.consuming = True
        while not self.stopped:
            polling_coro = loop.run_in_executor(None, self._poll)
            try:
                message = await polling_coro
            # We need a wide net cast, as there are several possible exceptions, and we don't
            # want any of them to cause the Consumer to crash, if left uncaught.
            except Exception as ex:  # pylint: disable=W0703
                # TODO: better error handling
                self._log.error("Error: %s", ex)
            else:
                if message:
                    await self.messages.put(message)
        self.consuming = False

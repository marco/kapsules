// Copyright (c) 2018 Kapsules.io.  All rights reserved.

package io.kapsules.gateway;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.URI;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GatewayApplicationTests {

  @Autowired
  ServiceLocator locator;

  @Test
  public void contextLoads() {
  }

  @Test
  public void canGetUri() {
    assertThat(locator, notNullValue());
    assertThat(locator.locateService("ingestion"), is(URI.create("http://ingestion:8080")));
  }
}

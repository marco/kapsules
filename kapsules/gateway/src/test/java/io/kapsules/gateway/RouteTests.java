// Copyright (c) 2018 Kapsules.io.  All rights reserved.

package io.kapsules.gateway;

import io.kapsules.gateway.predicates.GlobPathMatcherPredicate;
import org.junit.Test;
import org.springframework.http.server.RequestPath;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.server.ServerWebExchange;

import java.util.function.Predicate;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

//@RunWith(SpringRunner.class)
//@SpringBootTest
public class RouteTests {

  public static ServerWebExchange mockExchange(String path0, String... paths) {

    RequestPath requestPath = mock(RequestPath.class);
    when(requestPath.value()).thenReturn(path0, paths);

    ServerHttpRequest request = mock(ServerHttpRequest.class);
    when(request.getPath()).thenReturn(requestPath);

    ServerWebExchange mockExchange = mock(ServerWebExchange.class);
    when(mockExchange.getRequest()).thenReturn(request);

    return mockExchange;
  }

  @Test
  public void match() {
    ServerWebExchange ex = mockExchange(
        "/foo/bar/trigger",
        "/foo/bar/trigger/1234",
        "/foo/bar/trigger");

    Predicate<ServerWebExchange> matcher = new GlobPathMatcherPredicate("/**/trigger");
    assertTrue(matcher.test(ex));

    matcher = new GlobPathMatcherPredicate("/**/trigger/*");
    assertTrue(matcher.test(ex));
    assertFalse(matcher.test(ex));
  }

  @Test
  public void compoundPredicate() {
    ServerWebExchange ex = mockExchange(
        "/org/scope/trigger/my-trigger",
        "/trigger/1234",
        "/trigger/9876543-dead-beef/mapping/my-event");

    Predicate<ServerWebExchange> matcher = new GlobPathMatcherPredicate("/**/trigger/**")
        .or(new GlobPathMatcherPredicate("/trigger/**"));

    assertTrue(matcher.test(ex));
    assertTrue(matcher.test(ex));
    assertTrue(matcher.test(ex));
  }
}

// Copyright (c) 2018 Kapsules.io.  All rights reserved.

package io.kapsules.gateway;

import java.net.URI;

public interface ServiceLocator {

  URI locateService(String service);
}

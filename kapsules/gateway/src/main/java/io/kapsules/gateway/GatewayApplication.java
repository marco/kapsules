// Copyright (c) 2018 Kapsules.io.  All rights reserved.

package io.kapsules.gateway;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.URI;
import java.util.Map;

@Configuration
@SpringBootApplication
public class GatewayApplication {

  private final ServiceLocatorProperties serviceLocatorProperties;

  @Autowired
  public GatewayApplication(ServiceLocatorProperties serviceLocatorProperties) {
    this.serviceLocatorProperties = serviceLocatorProperties;
  }

  public static void main(String[] args) {
    SpringApplication.run(GatewayApplication.class, args);
  }

  @Bean ServiceLocator serviceLocator() {
    return new PropertyBackedServiceLocator(serviceLocatorProperties);
  }

}

// Copyright (c) 2018 Kapsules.io.  All rights reserved.

package io.kapsules.gateway;

import java.net.URI;
import java.util.Map;

public class PropertyBackedServiceLocator implements ServiceLocator {

  private ServiceLocatorProperties serviceUriMappings;

  public PropertyBackedServiceLocator(ServiceLocatorProperties serviceUriMappings) {
    this.serviceUriMappings = serviceUriMappings;
  }

  @Override
  public URI locateService(String service) {
    return serviceUriMappings.getServiceUri(service);
  }
}

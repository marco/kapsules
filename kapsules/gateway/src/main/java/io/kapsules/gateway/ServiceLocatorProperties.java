// Copyright (c) 2018 Kapsules.io.  All rights reserved.

package io.kapsules.gateway;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

@Component
@ConfigurationProperties(prefix = "services")
@Data
public class ServiceLocatorProperties {
  private final Map<String, URI> uriLocations = new HashMap<>();

  public URI getServiceUri(String service) {
    return uriLocations.get(service);
  }
}

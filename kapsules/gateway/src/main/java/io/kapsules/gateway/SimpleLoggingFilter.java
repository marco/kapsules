package io.kapsules.gateway;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.util.pattern.PathPattern;
import reactor.core.publisher.Mono;

import java.util.Map;

import static org.springframework.cloud.gateway.support.ServerWebExchangeUtils.URI_TEMPLATE_VARIABLES_ATTRIBUTE;

@Component
public class SimpleLoggingFilter implements GatewayFilter {

  private static final Logger log = LoggerFactory.getLogger(SimpleLoggingFilter.class);

  @Override
  public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {

    log.info("Method: {}, Host: {}, Path: {}, QueryParams: {}",
        exchange.getRequest().getMethod(),
        exchange.getRequest().getURI().getHost(),
        exchange.getRequest().getURI().getPath(),
        exchange.getRequest().getQueryParams());

    Map<String, ?> attributes = exchange.getAttributes();
    if (attributes.containsKey(URI_TEMPLATE_VARIABLES_ATTRIBUTE)) {
      PathPattern.PathMatchInfo info = (PathPattern.PathMatchInfo) attributes.get(
          URI_TEMPLATE_VARIABLES_ATTRIBUTE);
      log.debug("URI Path Variables: {}", info.getUriVariables());
    }

    return chain.filter(exchange);
  }
}

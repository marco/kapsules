// Copyright (c) 2018 Kapsules.io.  All rights reserved.

package io.kapsules.gateway;

import io.kapsules.gateway.predicates.GlobPathMatcherPredicate;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.URI;

import static org.slf4j.LoggerFactory.getLogger;


@Configuration
public class Routes {
  private static final Logger LOG = getLogger(Routes.class);

  public static final String FORWARDED_URL = "X-CF-Forwarded-Url";
  public static final String PROXY_METADATA = "X-CF-Proxy-Metadata";
  public static final String PROXY_SIGNATURE = "X-CF-Proxy-Signature";

  public static final String INGESTION_SERVICE = "ingestion";
  public static final String SCHEDULER_SERVICE = "scheduler";

  private final ServiceLocator serviceLocator;

  @Autowired
  public Routes(ServiceLocator serviceLocator) {
    this.serviceLocator = serviceLocator;
  }

  @Bean
  public RouteLocator customRouteLocator(RouteLocatorBuilder builder,
                                         SimpleLoggingFilter filter) {

    // TODO: replace hard-coded URIs with configured property and, eventually, with Eureka client
    return builder.routes()
        .route(r ->
            r.predicate(new GlobPathMatcherPredicate("/**/function/**"))
            .uri(serviceLocator.locateService(INGESTION_SERVICE))
        )
        .route(r ->
            r.predicate(new GlobPathMatcherPredicate("/**/trigger/**")
                .or(new GlobPathMatcherPredicate("/trigger/**")))
            .uri(serviceLocator.locateService(INGESTION_SERVICE))
        )
        .route(r ->
            r.path("/{org}/{scope}/event/{eventType}/source/{source}")
            .filters(f -> f.filter(filter))
            .uri(serviceLocator.locateService(SCHEDULER_SERVICE))
        )
        .build();
  }
}


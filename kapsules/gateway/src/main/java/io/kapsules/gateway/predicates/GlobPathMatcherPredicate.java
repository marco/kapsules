// Copyright (c) 2018 Kapsules.io.  All rights reserved.

package io.kapsules.gateway.predicates;

import org.slf4j.Logger;
import org.springframework.http.server.RequestPath;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.PathMatcher;
import org.springframework.web.server.ServerWebExchange;

import java.util.function.Predicate;

import static org.slf4j.LoggerFactory.getLogger;

public class GlobPathMatcherPredicate implements Predicate<ServerWebExchange> {

  private static final Logger LOG = getLogger(GlobPathMatcherPredicate.class);

  private final String pattern;
  private final PathMatcher matcher = new AntPathMatcher();

  public GlobPathMatcherPredicate(String pattern) {
    this.pattern = pattern;
  }

  @Override
  public boolean test(ServerWebExchange webExchange) {
    final String  path = webExchange.getRequest().getPath().value();

    boolean outcome = matcher.match(pattern, path);
    // TODO: move to trace() once usage is more stable.
    LOG.debug("Matching GLOB[{}] against '{}': {} matched", this.pattern, path,
        outcome ? "" : "not ");
    return outcome;
  }
}

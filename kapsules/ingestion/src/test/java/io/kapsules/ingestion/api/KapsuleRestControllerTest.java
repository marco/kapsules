// Copyright (c) 2018 Kapsules.io.  All rights reserved.

package io.kapsules.ingestion.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Maps;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;
import io.kapsules.core.ServerlessProtos;
import io.kapsules.domain.Kapsule;
import io.kapsules.ingestion.BaseSpringIntegrationTest;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.cassandraunit.spring.CassandraDataSet;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.data.cassandra.core.CassandraTemplate;
import org.springframework.data.cassandra.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.listener.MessageListener;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.springframework.data.cassandra.core.query.Criteria.where;


@CassandraDataSet(value = {"kapsule-base.cql", "functions.cql"})
public class KapsuleRestControllerTest extends BaseSpringIntegrationTest {

  private static final ObjectMapper MAPPER = new ObjectMapper();

  @Autowired
  private TestRestTemplate restTemplate;

  @Autowired
  private CassandraTemplate cassandraTemplate;

  @Autowired
  String domain;
  private ResponseEntity<String> response;

  @Before
  public void initialize() throws Exception {
    initializeEmbeddedKafka(BUILD_TOPIC);

    // setup a Kafka message listener
    container.setupMessageListener(new MessageListener<String, ServerlessProtos.Event>() {
      @Override
      public void onMessage(ConsumerRecord<String, ServerlessProtos.Event> record) {
        events.add(record.value());
      }
    });
    startEmbeddedKafka();
  }

  @After
  public void stop() {
    // stop the container
    container.stop();
  }


  @Test
  public void postFunction() throws IOException, InterruptedException {
    String uri = "/{org}/{scope}/function/{name}";

    Map<String, String> urlVariables = Maps.newHashMap();
    urlVariables.put("org", "mktg");
    urlVariables.put("scope", "revenues");
    urlVariables.put("name", "ad_spend");

    UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(uri)
        .queryParam("version", "14.0.36");

    ResponseEntity<String> response = this.restTemplate.postForEntity(
        builder.buildAndExpand(urlVariables).toUri(),
        "def ad_spend():\n"
            + "    print('This is real python code')\n"
            + "    exit(0)\n"
            + "",
        String.class);

    assertThat(response.getStatusCode(), is(HttpStatus.CREATED));

    Map<String, ?> body = MAPPER.readValue(response.getBody(), Map.class);
    assertTrue("Missing 'tag' key in response: " + response.getBody(),
        body.containsKey("tag"));
    assertThat(body.get("tag"), is(domain + "/mktg/revenues/ad_spend:14.0.36"));


    List<Kapsule> results = cassandraTemplate.select(
        Query.query(where("domain").is(domain))
            .and(where("org").is("mktg"))
            .and(where("scope").is("revenues"))
            .and(where("name").is("ad_spend")),
        Kapsule.class);
    assertThat(results.size(), is(1));
    assertThat(results.get(0).getTag(), is("testing/mktg/revenues/ad_spend:14.0.36"));
    assertThat(results.get(0).getBody(), containsString("This is real python"));

    assertThat(events.size(), is(1));
    ServerlessProtos.Event evt = events.take();
    assertThat(evt.getEventType(), is("build"));
  }

  /**
   * Tests that #160744746 got fixed. See: https://www.pivotaltracker.com/story/show/160744746
   *
   * @throws Exception if any error
   */
  @Test
  public void postFunctionWithDashes() throws Exception {
    String uri = "/{org}/{scope}/function/{name}";

    Map<String, String> urlVariables = Maps.newHashMap();
    urlVariables.put("org", "mktg");
    urlVariables.put("scope", "revenues");
    urlVariables.put("name", "my-func-awesome");

    UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(uri)
        .queryParam("version", "1.0.44");

    ResponseEntity<String> response = this.restTemplate.postForEntity(
        builder.buildAndExpand(urlVariables).toUri(),
        "def ad_spend():\n"
            + "    print('This is real python code')\n"
            + "    exit(0)\n"
            + "",
        String.class);
    assertThat(response.getStatusCode(), is(HttpStatus.CREATED));
  }

  @Test
  public void noVersionFails() {
    ResponseEntity<String> response = restTemplate.getForEntity("/dev/base/function/test-1",
        String.class);
    assertThat(response.getStatusCode(), is(HttpStatus.BAD_REQUEST));
    assertThat(response.getBody(), containsString("version must be specified"));
  }

  @Test
  public void getFunction() throws InvalidProtocolBufferException {
    String tag = domain + "/dev/base/test-1:1.0.1-b38";
    String body = this.restTemplate.getForObject("/dev/base/function/test-1?version=1.0.1-b38",
        String.class);

    ServerlessProtos.Function.Builder functionBuilder = ServerlessProtos.Function.newBuilder();
    JsonFormat.parser().ignoringUnknownFields().merge(body, functionBuilder);

    String actualTag = functionBuilder.build().getTag();
    assertThat(actualTag, is(tag));
  }

  @Test
  public void getAllFunctionsForOrgScope() throws IOException {
    String body = this.restTemplate.getForObject("/dev/qa/function/", String.class);
    List<?> tags = MAPPER.<List>readValue(body, List.class);

    assertThat(tags, containsInAnyOrder("testing/dev/qa/test-2:2.0.434",
        "testing/dev/qa/test-3:1.0"));
  }

  @Test
  public void getFunctionBody() {
    String body = restTemplate.getForObject("/dev/base/function/test-1/body?version=1.1",
        String.class);
    assertThat(body, is("IF(IA.EQ.0 .OR. IB.EQ.0 .OR. IC.EQ.0) STOP 1"));
  }
}

// Copyright (c) 2018 Kapsules.io.  All rights reserved.

package io.kapsules.ingestion;

import io.kapsules.common.config.CassandraConfiguration;
import io.kapsules.common.config.KafkaConfiguration;
import io.kapsules.common.utils.ProtoUtils;
import io.kapsules.core.ServerlessProtos;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(
    classes = {
        ApplicationConfiguration.class,
        CassandraConfiguration.class,
        KafkaConfiguration.class
    },
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class IngestionApplicationTest /*extends BaseSpringIntegrationTest*/ {

  @Autowired
  String domain;

  @Autowired
  ServerlessProtos.Source self;

  // TODO: confirm this is no longer necessary:
  //  @Ignore("Causes the dreaded 'eventTopicMappings exist' exception")
  @Test
  public void contextLoads() {
    assertThat(domain, is("testing"));
    assertEquals(self.getNamespace(),
        ProtoUtils.Namespace.of("testing-system-server").build());
    assertThat(self.getName(), is("ingestion-server"));
  }
}

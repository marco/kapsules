// Copyright (c) 2018 Kapsules.io.  All rights reserved.

package io.kapsules.ingestion.api;

import com.datastax.driver.core.querybuilder.Insert;
import com.datastax.driver.core.querybuilder.QueryBuilder;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Maps;
import io.kapsules.common.utils.ProtoUtils;
import io.kapsules.core.ServerlessProtos;
import io.kapsules.core.ServerlessProtos.State;
import io.kapsules.domain.NamespaceAndNameKey;
import io.kapsules.domain.Trigger;
import io.kapsules.domain.TriggerByName;
import io.kapsules.domain.TriggerBySource;
import io.kapsules.ingestion.BaseSpringIntegrationTest;
import org.cassandraunit.spring.CassandraDataSet;
import org.junit.*;
import org.junit.rules.Timeout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.data.cassandra.core.CassandraTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.listener.MessageListener;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.test.StepVerifier;

import java.io.IOException;
import java.net.URI;
import java.nio.ByteBuffer;
import java.time.Duration;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static io.kapsules.core.ServerlessProtos.State.*;
import static io.kapsules.ingestion.Constants.INGESTION_SERVER;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

@CassandraDataSet(value = {"kapsule-base.cql", "triggers.cql"})
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
@SuppressWarnings("unchecked")
public class TriggerRestControllerTest extends BaseSpringIntegrationTest {

  private static final ObjectMapper MAPPER = new ObjectMapper();
  private static final String TEST_FUNC = "eng/dev/test-func:0.1.0";

  @Rule
  public Timeout timeout = new Timeout(15, SECONDS);

  @Autowired
  private TestRestTemplate restTemplate;

  @Autowired
  private CassandraTemplate cassandraTemplate;

  @Autowired
  private TriggerRestController controller;

  @Autowired
  String domain;

  @Before
  public void initialize() throws Exception {
    initializeEmbeddedKafka(TRIGGER_TOPIC);

    // setup a Kafka message listener
    container.setupMessageListener((MessageListener<String, ServerlessProtos.Event>)
        record -> events.add(record.value()));
    startEmbeddedKafka();

    StepVerifier.setDefaultTimeout(Duration.ofMillis(200));
  }

  @After
  public void stop() {
    // Drain the Kafka events topic, so as to avoid polluting subsequent tests.
    List<ServerlessProtos.Event> sink = new LinkedList<>();
    events.drainTo(sink);
    container.stop();
  }

  /**
   * We need to emulate the server's Trigger creation, which does a "join on write" when creating a
   * new trigger; so that, when the PUT executes, we will find items in place: for that we need to
   * generate the Protobuf and serialize it.
   */
  private TriggerBySource insertTriggerBySourceIntoTriggers(NamespaceAndNameKey pk) {
    TriggerBySource trigger = cassandraTemplate.selectOneById(pk, TriggerBySource.class);
    assertThat(trigger, notNullValue());

    ServerlessProtos.Trigger pbTrigger = ServerlessProtos.Trigger.newBuilder()
        .setId(trigger.getTriggerId().toString())
        .setTriggerSource(
            ProtoUtils.Source.of(trigger.getSource().toNamespace(), trigger.getSource().getName())
        )
        .setState(State.valueOf(trigger.getState()))
        .putAllEventTriggerMapping(trigger.toFunctionMapping())
        .build();

    cassandraTemplate.insert(Trigger.fromProto(pbTrigger));
    cassandraTemplate.insert(new TriggerByName(pk, trigger.getTriggerId()));

    return trigger;
  }

  @Test
  public void postTrigger() throws IOException, InterruptedException {
    String uri = "/{org}/{scope}/trigger/{name}";

    Map<String, String> urlVariables = Maps.newHashMap();
    urlVariables.put("org", "eng");
    urlVariables.put("scope", "dev");
    urlVariables.put("name", "test-trigger");

    UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(uri);

    String body =
        "{"
            + "  \"source_name\": \"commit\","
            + "  \"description\": \"A test to POST to the Trigger\","
            + "  \"state\": \"ENABLED\",\"event_mapping\": {"
            + "    \"earthquake\": \"emergency/save/ass:1.0\","
            + "    \"flood\": \"emergency/swim/fast:2.0.1\""
            + "  }"
            + "}";
    HttpEntity<Map<String, ?>> requestBody = new HttpEntity<>(MAPPER.readValue(body, Map.class));

    ResponseEntity<String> response = restTemplate.postForEntity(
        builder.buildAndExpand(urlVariables).toUri(),
        requestBody,
        String.class);

    assertThat(response.toString(),
        response.getStatusCode(), is(HttpStatus.CREATED));

    // The event is sent in a background thread, so we must wait for it to appear.
    // The Timeout rule will take care of failing this test if it doesn't appear within a
    // reasonable time.
    while (events.isEmpty()) {
      MILLISECONDS.sleep(100);
    }

    ServerlessProtos.Event event = events.take();
    assertThat(event.getEventType(), is("new_trigger"));
    assertThat(event.getNamespace().getOrg(), is("eng"));
    assertThat(event.getSource().getName(), is(INGESTION_SERVER));
    ServerlessProtos.Trigger trigger = ServerlessProtos.Trigger.parseFrom(event.getPayload());

    // noinspection ConstantConditions
    assertThat(response.getHeaders().getLocation().getPath(),
        equalTo(String.format("/trigger/%s", trigger.getId())));
  }

  @Test
  public void postTriggerSavesToAllTables() throws IOException, InterruptedException {
    String uri = "/{org}/{scope}/trigger/{name}";

    Map<String, String> urlVariables = Maps.newHashMap();
    urlVariables.put("org", "mktg");
    urlVariables.put("scope", "promotions");
    urlVariables.put("name", "mktg-trigger");

    UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(uri);

    String body = "{"
        + "  \"source_name\": \"my-source\","
        + "  \"state\": \"ENABLED\","
        + "  \"event_mapping\": {"
        + "    \"test\": \"mktg/campaigns/revert:1.0\""
        + "  }"
        + "}";
    HttpEntity<Map<String, ?>> requestBody = new HttpEntity<>(MAPPER.readValue(body, Map.class));

    ResponseEntity<String> response = restTemplate.postForEntity(
        builder.buildAndExpand(urlVariables).toUri(),
        requestBody,
        String.class);

    while (events.isEmpty()) {
      MILLISECONDS.sleep(100);
    }
    ServerlessProtos.Event event = events.take();
    assertThat(event.getEventType(), is("new_trigger"));


    assertThat(response.toString(),
        response.getStatusCode(), is(HttpStatus.CREATED));

    // noinspection ConstantConditions
    String path = response.getHeaders().getLocation().getPath();
    String uuid = path.substring(path.lastIndexOf('/') + 1);

    // Here we want to confirm that the data was correctly saved in Cassandra.
    Trigger t = cassandraTemplate.selectOneById(UUID.fromString(uuid), Trigger.class);
    assertThat(t, notNullValue());
    ServerlessProtos.Trigger trigger = ServerlessProtos.Trigger.parseFrom(t.getSerializedPb());

    assertThat(trigger.getName(), is("mktg-trigger"));

    ServerlessProtos.Source source = trigger.getTriggerSource();
    NamespaceAndNameKey key = NamespaceAndNameKey.fromSource(source);
    TriggerBySource tbs = cassandraTemplate.selectOneById(key, TriggerBySource.class);
    assertThat(tbs, notNullValue());
    assertThat(tbs.getTriggerId().toString(), is(trigger.getId()));

    key.setName("mktg-trigger");
    TriggerByName tbn = cassandraTemplate.selectOneById(key, TriggerByName.class);
    assertThat(tbn, notNullValue());
    assertThat(tbn.getTriggerId().toString(), is(trigger.getId()));
    assertThat(tbn.getNameKey().getName(), is("mktg-trigger"));
  }

  @Test
  public void getTriggerById() {
    UUID id = UUID.randomUUID();
    ServerlessProtos.Trigger pb = ServerlessProtos.Trigger.newBuilder()
        .setId(id.toString())
        .setName("test_trigger")
        .build();

    Trigger trigger = Trigger.fromProto(pb);
    cassandraTemplate.insert(trigger);

    ResponseEntity<Map> response = restTemplate.getForEntity("/trigger/" + id.toString(), Map
        .class);

    assertThat(response.getStatusCode(), is(HttpStatus.OK));
    assertThat(response.getBody(), notNullValue());
    assertThat(response.getBody().get("id"), is(id.toString()));
    assertThat(response.getBody().get("name"), is("test_trigger"));
  }

  /**
   * What we want to verify with this test is that the server won't panic if there are malformed PBs
   * introduced (for whatever reason) in the system.
   */
  @Test
  public void getTriggerByIdForMalformedProtoReturns404() {
    // TODO: we should post an error message in a "system_cleanup" event queue, upon discovering
    // the error.
    UUID id = UUID.randomUUID();
    Insert insertBuilder = QueryBuilder.insertInto("triggers")
        .value("trigger_id", id)
        .value("serializedpb", ByteBuffer.wrap("Definitely not a Protobuf".getBytes()));

    cassandraTemplate.getCqlOperations().execute(insertBuilder);

    ResponseEntity<Map> response = restTemplate.getForEntity("/trigger/" + id.toString(), Map
        .class);
    assertThat(response.getStatusCode(), is(HttpStatus.NOT_FOUND));
  }

  @Test
  public void getTriggerByIdNotExistsReturnsNotFound() {
    UUID id = UUID.randomUUID();
    ResponseEntity<Map> response = restTemplate.getForEntity("/trigger/" + id.toString(), Map
        .class);
    assertThat(response.getStatusCode(), is(HttpStatus.NOT_FOUND));
  }

  @Test
  public void getNamedTriggersForOrgScope() {
    // We need to populate the entry in the table programmatically, because it expects a
    // serialized copy of the Protobuf.
    final String id = "5f10e105-21eb-4ea0-ac21-4102e25f5f96";
    final String triggerName = "revenues-trigger";
    final String orgName = "sales";
    final String scope = "apac";

    ServerlessProtos.Trigger pb = ServerlessProtos.Trigger.newBuilder()
        .setId(id)
        .setName(triggerName)
        .setTriggerSource(ProtoUtils.Source.of(
            ProtoUtils.Namespace.of("domain", orgName, scope).build(),
            "test-source"
        ))
        .build();
    cassandraTemplate.insert(Trigger.fromProto(pb));

    String uri = "/{org}/{scope}/trigger/{name}";

    Map<String, String> urlVariables = Maps.newHashMap();
    urlVariables.put("org", orgName);
    urlVariables.put("scope", scope);
    urlVariables.put("name", triggerName);

    UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(uri);

    ResponseEntity<Map> response = restTemplate.getForEntity(
        builder.buildAndExpand(urlVariables).toUri(),
        Map.class
    );

    assertThat(response.getStatusCode(), is(HttpStatus.OK));
    Map<String, ?> data = response.getBody();
    assertThat(data, notNullValue());
    assertThat(data.get("id"), is(id));
    assertThat(data.get("name"), is(triggerName));

    Map<String, String> ns = (Map<String, String>) (
        (Map<String, ?>) data.get("triggerSource")).get("namespace");
    assertThat(ns.get("org"), is(orgName));
    assertThat(ns.get("scope"), is(scope));
  }

  @Test
  public void addMappings() throws InterruptedException {

    NamespaceAndNameKey pk = new NamespaceAndNameKey(domain, "qa",
        "testing", "releases");

    // This is only necessary as we are NOT populating the `triggers` table before the test starts.
    TriggerBySource trigger = insertTriggerBySourceIntoTriggers(pk);

    Map<String, String> mappings = Maps.newHashMap();
    mappings.put("fire", "alarm/estinguish:1.0.1");
    mappings.put("big_fire", "alarm/run:2.0.1");

    restTemplate.put("/trigger/{id}/mapping", mappings, trigger.getTriggerId().toString());

    while (events.isEmpty()) {
      MILLISECONDS.sleep(100);
    }
    ServerlessProtos.Event event = events.take();
    assertThat(event.getEventType(), is("updated_trigger"));

    // noinspection ConstantConditions
    while (trigger.getEventToFunctionMap().size() < 3) {
      trigger = cassandraTemplate.selectOneById(pk, TriggerBySource.class);
    }
    assertThat(trigger.getEventToFunctionMap().get("big_fire"), is("alarm/run:2.0.1"));
  }


  @Test
  public void removeMapping() {
    NamespaceAndNameKey pk = new NamespaceAndNameKey(domain, "eng", "dev", "integration");

    // This is only necessary as we are NOT populating the `triggers` table before the test starts.
    TriggerBySource trigger = insertTriggerBySourceIntoTriggers(pk);
    assertThat(trigger.getEventToFunctionMap().size(), is(2));

    RequestEntity<?> request = RequestEntity.delete(URI.create(
        String.format("/trigger/%s/mapping/%s", trigger.getTriggerId().toString(),
            "will_delete"))).build();

    ResponseEntity<?> response = restTemplate.exchange(request, Object.class);
    assertThat(response.getStatusCode(), is(HttpStatus.RESET_CONTENT));

    trigger = cassandraTemplate.selectOneById(pk, TriggerBySource.class);
    assertThat(trigger, notNullValue());
    assertThat(trigger.getEventToFunctionMap().size(), is(1));
    assertFalse(trigger.getEventToFunctionMap().containsKey("will_delete"));
    assertTrue("Found instead: " + trigger.getEventToFunctionMap().keySet(),
        trigger.getEventToFunctionMap().containsKey("can_stay"));
  }

  @Test
  public void removeNonExistingMappingReturns400() {
    NamespaceAndNameKey pk = new NamespaceAndNameKey(domain, "eng",
        "dev", "integration");
    TriggerBySource trigger = insertTriggerBySourceIntoTriggers(pk);
    assertThat(trigger.getEventToFunctionMap().size(), is(2));

    RequestEntity<?> request = RequestEntity.delete(URI.create(
        String.format("/trigger/%s/mapping/%s", trigger.getTriggerId().toString(),
            "fake-mapping"))).build();

    ResponseEntity<?> response = restTemplate.exchange(request, Object.class);
    assertThat(response.getStatusCode(), is(HttpStatus.BAD_REQUEST));
  }

  @Test
  public void removeMappingFromNonExistingTriggerReturns404() {

    RequestEntity<?> request = RequestEntity.delete(URI.create(
        String.format("/trigger/%s/mapping/%s", UUID.randomUUID(),
            "fake-mapping"))).build();

    ResponseEntity<?> response = restTemplate.exchange(request, Object.class);
    assertThat(response.getStatusCode(), is(HttpStatus.NOT_FOUND));
  }

  @Test
  @Ignore("Temporarily disabled, fails when pausing the trigger")
  public void canChangeState() throws InterruptedException {
    UUID id = UUID.fromString("5f10e105-21eb-4ea0-ac21-4102e25f5e00");
    NamespaceAndNameKey key = new NamespaceAndNameKey(domain, "eng", "testing",
        "state-change");

    TriggerBySource tbs = insertTriggerBySourceIntoTriggers(key);
    assertThat(tbs.getTriggerId(), is(id));

    // Change the state via PUT API:
    //   /trigger/{uuid}/state/{new_state}?paused_until={time_millis}
    restTemplate.put(String.format("/trigger/%s/state/PAUSED?paused_until=1609487940000", id), "");

    Thread.sleep(500);
    Trigger trigger = cassandraTemplate.selectOneById(id, Trigger.class);
    assertThat(trigger, notNullValue());

    ServerlessProtos.Trigger pb = trigger.toProto().block();
    assertThat(pb, notNullValue());

    assertThat(pb.getState(), is(PAUSED));
    assertThat(pb.getPausedUntil(), is(1609487940000L));

    restTemplate.put(String.format("/trigger/%s/state/DISABLED", id), "");

    trigger = cassandraTemplate.selectOneById(id, Trigger.class);
    assertThat(trigger, notNullValue());
    pb = trigger.toProto().block();
    assertThat(pb, notNullValue());
    assertThat(pb.getState(), is(DISABLED));
  }

  @Test
  public void testSave() throws InterruptedException {
    ServerlessProtos.Namespace ns = ProtoUtils.Namespace.of("testing-eng-dev").build();
    ServerlessProtos.Source source = ProtoUtils.Source.of(ns, "test-source").build();

    ServerlessProtos.Trigger pbTrig = ServerlessProtos.Trigger.newBuilder()
        .setId(UUID.randomUUID().toString())
        .setName("Test")
        .setTriggerSource(source)
        .setState(ENABLED)
        .setDescription("A simple test trigger")
        .putEventTriggerMapping("event", ServerlessProtos.Function.newBuilder()
            .setTag(TEST_FUNC)
            .build())
        .build();

    assertThat(controller, notNullValue());
    StepVerifier
        .create(controller.save(pbTrig))
        .expectNext(Trigger.fromProto(pbTrig))
        .verifyComplete();

    NamespaceAndNameKey bySource = new NamespaceAndNameKey(
        ns.getDomain(), ns.getOrg(), ns.getScope(), source.getName());

    // We need to wait a beat for the data to be persisted; while this may seem an infinite loop,
    // the Timeout @Rule will take care of failing the test.
    TriggerBySource tbs = null;
    while (tbs == null) {
      tbs = cassandraTemplate.selectOneById(bySource, TriggerBySource.class);
      Thread.sleep(50);
    }
    assertThat(tbs, notNullValue());
    assertThat(tbs.getEventToFunctionMap().get("event"), is(TEST_FUNC));

    NamespaceAndNameKey byName = new NamespaceAndNameKey(ns.getDomain(), ns.getOrg(), ns.getScope(),
        pbTrig.getName());
    TriggerByName tbn = cassandraTemplate.selectOneById(byName, TriggerByName.class);
    assertThat(tbn, notNullValue());
    assertThat(tbn.getTriggerId(), is(UUID.fromString(pbTrig.getId())));
  }
}

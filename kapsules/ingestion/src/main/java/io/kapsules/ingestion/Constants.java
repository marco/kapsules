// Copyright (c) 2018 Kapsules.io.  All rights reserved.

package io.kapsules.ingestion;

public class Constants {

  // TODO: create a single CqlQueriesConstants class shared by all projects
  public static final String KEYSPACE = "serverless";
  public static final String KAPSULES_TABLE = KEYSPACE + ".kapsules";
  public static final String TRIGGERS_TABLE = KEYSPACE + ".triggers";

  public static final String SELECT_FUNCTION_BY_ORG_SCOPE = "SELECT tag FROM " + KAPSULES_TABLE
      + " WHERE domain = '%s' AND org = '%s' AND scope = '%s';";

  // API JSON Body Key Mappings
  //
  public static final String PAUSED_UNTIL = "paused_until";
  public static final String EVENT_MAPPING = "event_mapping";

  public static final String KAFKA_GROUP_ID = "ingestion_group";
  public static final String INGESTION_SERVER = "ingestion-server";
}

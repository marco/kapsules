// Copyright (c) 2018 Kapsules.io.  All rights reserved.

package io.kapsules.ingestion.api;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.Maps;
import io.kapsules.common.utils.ProtoUtils;
import io.kapsules.core.ServerlessProtos;
import io.kapsules.core.ServerlessProtos.State;
import io.kapsules.domain.NamespaceAndNameKey;
import io.kapsules.domain.Trigger;
import io.kapsules.domain.TriggerByName;
import io.kapsules.domain.TriggerBySource;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.cassandra.core.ReactiveCassandraTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.lang.Nullable;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import static io.kapsules.common.SharedConstants.NEW_TRIGGER_EVENT;
import static io.kapsules.common.SharedConstants.UPDATED_TRIGGER_EVENT;
import static io.kapsules.common.utils.ProtoUtils.*;
import static io.kapsules.ingestion.Constants.EVENT_MAPPING;
import static io.kapsules.ingestion.Constants.PAUSED_UNTIL;
import static io.kapsules.ingestion.api.TriggerRestController.MappingOperation.REMOVE;
import static io.kapsules.ingestion.api.TriggerRestController.MappingOperation.UPDATE;
import static org.slf4j.LoggerFactory.getLogger;

@RestController
@RequestMapping
public class TriggerRestController {

  public enum MappingOperation {
    UPDATE, REMOVE, REMOVE_ALL
  }


  private static final Logger LOG = getLogger(TriggerRestController.class);

  private final String domain;
  private final ReactiveCassandraTemplate template;
  private final KafkaTemplate<Long, ServerlessProtos.Event> producer;
  private final ServerlessProtos.Source self;

  @Value("#{triggerTopic}")
  String topic;

  @Autowired
  public TriggerRestController(
      String domain,
      ServerlessProtos.Source self,
      ReactiveCassandraTemplate template,
      KafkaTemplate<Long, ServerlessProtos.Event> producer
  ) {
    this.domain = domain;
    this.template = template;
    this.producer = producer;
    this.self = self;
  }


  @PostMapping("/{org}/{scope}/trigger/{name}")
  public Mono<ResponseEntity<?>> postFunction(
      @PathVariable String org,
      @PathVariable String scope,
      @PathVariable String name,
      @RequestBody Map<String, ?> jsonBody
  ) {

    ServerlessProtos.Namespace namespace = ProtoUtils.Namespace.of(domain, org, scope).build();

    if (StringUtils.isEmpty(jsonBody.get("source_name"))) {
      return Mono.just(
          ResponseEntity.badRequest()
              .body(Collections.singletonMap("msg", "source_name is required when "
                  + "creating a Trigger")));
    }

    String sourceName = jsonBody.get("source_name").toString();
    ServerlessProtos.Source source = ProtoUtils.Source.of(namespace, sourceName).build();

    UUID id = UUID.randomUUID();
    String description = Optional.ofNullable(jsonBody.get("description"))
        .map(Object::toString).orElse("");

    ServerlessProtos.State state = Optional.ofNullable(jsonBody.get("state"))
        .map(x -> ServerlessProtos.State.valueOf((String) x))
        .orElse(ServerlessProtos.State.ENABLED);

    ServerlessProtos.Trigger.Builder newTrigger = ServerlessProtos.Trigger.newBuilder()
        .setId(id.toString())
        .setName(name)
        .setDescription(description)
        .setTriggerSource(source)
        .setState(state);

    if (state.equals(ServerlessProtos.State.PAUSED)) {
      if (!jsonBody.containsKey(PAUSED_UNTIL)) {
        LOG.error("Paused trigger missing `" + PAUSED_UNTIL + "` timestamp in request: {}",
            jsonBody);
        return Mono.just(ResponseEntity.badRequest().body(makeJsonError(
            "Missing 'paused_until' field",
            HttpStatus.BAD_REQUEST.value(),
            "PAUSED trigger MUST have `paused_until` field in request body, indicating "
                + "in ms from epoch when to re-enable it.")));
      }
      newTrigger.setPausedUntil((Long) jsonBody.get(PAUSED_UNTIL));
    }

    // This is the mapping from Event.eventType to Function.tag; MUST be present.
    if (!jsonBody.containsKey(EVENT_MAPPING)) {
      LOG.error("Missing `event_mapping` map from event type to function tag in request body: {}",
          jsonBody);
      return Mono.just(ResponseEntity.badRequest().body(makeJsonError(
          "Missing 'event_mapping' field",
          HttpStatus.BAD_REQUEST.value(),
          "Request MUST contain a mapping from event type to function tag")));
    }

    // noinspection unchecked -- nothing can be done here to make IntelliJ happy, so we just shut
    // it up
    ((Map<String, String>) jsonBody.get(EVENT_MAPPING)).forEach((key, value) -> {
      ServerlessProtos.Function f = ServerlessProtos.Function.newBuilder()
          .setNs(namespace)
          .setTag(value)
          .build();
      newTrigger.putEventTriggerMapping(key, f);
    });

    final ServerlessProtos.Trigger trigger = newTrigger.build();
    return save(trigger)
        .doOnSuccess(triggerBySource -> sendTriggerEvent(trigger, NEW_TRIGGER_EVENT))
        .map(tbs ->
            ResponseEntity.created(makeUri(id))
                .body(toJson(trigger)));
  }

  private URI makeUri(UUID id) {
    UriComponentsBuilder builder = UriComponentsBuilder.fromUriString("/trigger/{uuid}");

    Map<String, String> uriComponents = Maps.newHashMap();
    uriComponents.put("uuid", id.toString());
    return builder.buildAndExpand(uriComponents).toUri();
  }

  @GetMapping("/trigger/{uuid}")
  public Mono<ResponseEntity<String>> getTriggerById(@PathVariable UUID uuid) {
    return template.selectOneById(uuid, Trigger.class)
        .flatMap(Trigger::toProto)
        .map(ProtoUtils::toJson)
        .map(ResponseEntity::ok)
        .defaultIfEmpty(ResponseEntity.notFound().build());
  }

  @GetMapping("/{org}/{scope}/trigger/{name}")
  public Mono<ResponseEntity<String>> getNamedTriggerForOrgScope(
      @PathVariable String org,
      @PathVariable String scope,
      @PathVariable String name
  ) {
    NamespaceAndNameKey pk = new NamespaceAndNameKey(domain, org, scope, name);

    return template.selectOneById(pk, TriggerByName.class)
        .flatMap(tbn -> {
          UUID uuid = tbn.getTriggerId();
          return template.selectOneById(uuid, Trigger.class)
              .flatMap(trigger -> quietlyParse(trigger.getSerializedPb(),
                  ServerlessProtos.Trigger.class));
        })
        .map(ProtoUtils::toJson)
        .doOnSuccess(LOG::debug)
        .map(ResponseEntity::ok)
        .defaultIfEmpty(ResponseEntity.notFound().build());
  }

  @PutMapping("/trigger/{uuid}/mapping")
  public Mono<ResponseEntity<String>> addMappings(@PathVariable UUID uuid,
                                                  @RequestBody Map<String, String> mappings) {
    return template.selectOneById(uuid, Trigger.class)
        .flatMap(trigger -> updateMappings(trigger, mappings, UPDATE))
        .doOnSuccess(trigger -> {
          if (trigger != null) {
            trigger.toProto()
                .subscribe(t -> sendTriggerEvent(t, UPDATED_TRIGGER_EVENT));
          }
        })
        .flatMap(trigger -> updateMappingsBySource(trigger, mappings, UPDATE))
        .map(t -> ResponseEntity.accepted().body("{\"msg\": \"Adding mappings to Trigger\"}"))
        .defaultIfEmpty(ResponseEntity.notFound().build());
  }

  @PutMapping("/trigger/{uuid}/state/{newState}")
  public Mono<ResponseEntity<Object>> changeTriggerState(
      @PathVariable UUID uuid,
      @PathVariable ServerlessProtos.State newState,
      @Nullable @RequestParam Long pausedUntil
  ) {
    if (State.PAUSED.equals(newState) && (pausedUntil == null)) {
      return Mono.just(ResponseEntity.badRequest().body("When state is PAUSED the value "
          + "of `paused_until` query arg MUST be given (as a valid millisecond time since epoch)"));
    }

    return template.selectOneById(uuid, Trigger.class)
        .map(Trigger::toProto)
        .doOnSuccess(tp -> {
          tp.subscribe(trig -> {
            ServerlessProtos.Trigger pbtrig = ServerlessProtos.Trigger.newBuilder(trig)
                .setState(newState)
                .setPausedUntil(pausedUntil != null ? pausedUntil : 0L)
                .build();

            template.update(Trigger.fromProto(pbtrig))
                .doOnError(thr -> LOG.error("Could not update trigger state: {}",
                    thr.getLocalizedMessage()))

                .doOnSuccess(t ->
                    template.update(TriggerBySource.fromProto(pbtrig))
                        .doOnError(thr -> LOG.error("Could not update trigger state: {}",
                            thr.getLocalizedMessage()))
                        .doOnSuccess(tbs -> sendTriggerEvent(pbtrig, UPDATED_TRIGGER_EVENT))
                        .subscribe()
                )
                .subscribe();
          });
        })
        .map(t -> ResponseEntity.status(HttpStatus.RESET_CONTENT).build())
        .defaultIfEmpty(ResponseEntity.notFound().build());
  }

  @DeleteMapping("/trigger/{triggerId}/mapping/{eventType}")
  public Mono<?> removeMapping(@PathVariable UUID triggerId,
                               @PathVariable String eventType) {
    Map<String, String> mappings = Collections.singletonMap(eventType, "");

    return template.selectOneById(triggerId, Trigger.class)
        .flatMap(trigger -> updateMappings(trigger, mappings, REMOVE))
        .flatMap(trigger -> updateMappingsBySource(trigger, mappings, REMOVE))
        .map(t -> ResponseEntity.status(HttpStatus.RESET_CONTENT).build())
        .onErrorReturn(IllegalArgumentException.class,
            ResponseEntity.badRequest()
                .body(Collections.singletonMap("error",
                    "Cannot remove mapping for eventType: " + eventType)))
        .defaultIfEmpty(ResponseEntity.notFound().build());
  }

  private Mono<Trigger> updateMappings(
      Trigger trigger,
      Map<String, String> mappings,
      MappingOperation op
  ) {
    return trigger.toProto()
        .flatMap(t -> {
          ServerlessProtos.Trigger.Builder pb =
              ServerlessProtos.Trigger.newBuilder(t);
          switch (op) {
            case UPDATE:
              mappings.forEach((eventType, tag) -> pb.putEventTriggerMapping(eventType,
                  ServerlessProtos.Function.newBuilder()
                      .setNs(pb.getTriggerSource().getNamespace())
                      .setTag(tag)
                      .build()));
              break;
            case REMOVE:
              for (String key : mappings.keySet()) {
                if (!pb.containsEventTriggerMapping(key)) {
                  throw new IllegalArgumentException("Cannot remove non existing eventType: "
                      + key);
                }
              }
              mappings.forEach((eventType, tag) -> pb.removeEventTriggerMapping(eventType));
              break;
            case REMOVE_ALL:
              pb.clearEventTriggerMapping();
              break;
            default:
              LOG.error("Unexpected MappingOperation value: {}", op);
              throw new IllegalArgumentException("Unexpected MappingOperation value: " + op);
          }
          return template.update(Trigger.fromProto(pb.build()));
        });
  }

  private Mono<TriggerBySource> updateMappingsBySource(
      Trigger trigger,
      Map<String, String> mappings,
      MappingOperation op
  ) {
    return trigger.toProto()
        .flatMap(pbTrig -> {
          ServerlessProtos.Source source = pbTrig.getTriggerSource();

          // Now update the mapping kept in the `trigger_by_source` table
          NamespaceAndNameKey pk = NamespaceAndNameKey.fromSource(source);
          return template.selectOneById(pk, TriggerBySource.class)
              .flatMap(triggerBySource -> {
                switch (op) {
                  case UPDATE:
                    mappings.forEach((eventType, functionTag) ->
                        triggerBySource.getEventToFunctionMap().put(eventType, functionTag));
                    break;
                  case REMOVE_ALL:
                    triggerBySource.getEventToFunctionMap().clear();
                    break;
                  case REMOVE:
                    mappings.forEach((eventType, tag) -> {
                      triggerBySource.getEventToFunctionMap().remove(eventType);
                    });
                    break;
                  default:
                    throw new IllegalArgumentException("Unexpected operator '" + op + "'");
                }
                return template.update(triggerBySource);
              });
        });
  }

  private void sendTriggerEvent(ServerlessProtos.Trigger trigger, String eventType) {
    ServerlessProtos.Event event = ProtoUtils.Event.of(
        trigger.getTriggerSource().getNamespace(),
        self,
        eventType
    )
        .setId(UUID.randomUUID().toString())
        .setPayload(trigger.toByteString())
        .setSerializedAs(ServerlessProtos.Event.SerializedAs.PROTOBUF)
        .build();

    LOG.debug("Sending Event \n--\n{}\n--\nto topic: '{}'", event, topic);

    producer.send(topic, event).addCallback(
        result -> {
          // noinspection ConstantConditions - shut the warning about possible NPE on result.
          LOG.debug("Published event type = {}, topic = {}, offset = {}",
              event.getEventType(), topic, result.getRecordMetadata().offset());
        },
        ex -> {
          LOG.error("Failed to send Trigger event, type = {}, name = {}, cause = {}",
              eventType, trigger.getName(), ex.getLocalizedMessage());
          // TODO: does it make sense to try and publish to an error topic? clearly kafka is
          // clearly having troubles here.
          // TODO: add failure metrics here.
        });
  }

  @VisibleForTesting
  Mono<Trigger> save(ServerlessProtos.Trigger trigger) {
    LOG.debug("Saving Trigger: {}; State: {}; Source: {}",
        trigger.getName(),
        trigger.getState(),
        trigger.getTriggerSource().getName()
    );

    return template.insert(Trigger.fromProto(trigger))
        .doOnSuccess(t ->
            template.insert(TriggerBySource.fromProto(trigger))
                .doOnSuccess(tbs -> template.insert(TriggerByName.fromProto(trigger))
                    .subscribe())
                .subscribe()
        )
        .doOnError(throwable -> LOG.error("Could not save Trigger [{}]: {}",
            trigger.getId(), throwable.getLocalizedMessage()));
  }
}

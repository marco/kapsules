// Copyright (c) 2018 Kapsules.io.  All rights reserved.

package io.kapsules.ingestion.messaging;

import io.kapsules.core.ServerlessProtos;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.function.Consumer;

import static io.kapsules.common.SharedConstants.*;
import static io.kapsules.ingestion.Constants.KAFKA_GROUP_ID;

/**
 * This consumer is used to ingest functions and triggers that are created via Kafka events, instead
 * of REST API calls.
 *
 * <p>A new function/trigger will be posted to this queue as an
 * {@link io.kapsules.core.ServerlessProtos.Event} message, with an {@literal eventType} equal to
 * either {@link io.kapsules.common.SharedConstants#NEW_TRIGGER_EVENT} or {@link
 * io.kapsules.common.SharedConstants#NEW_FUNCTION_EVENT}, and a {@literal payload} as appropriate.
 *
 * <p>The newly created function/trigger will be then saved to Cassandra and a (cascading) event
 * will be generated on either the {@literal serverless.build} or {@literal serverless.trigger}
 * topic, as necessary.
 *
 * @see io.kapsules.ingestion.api.KapsuleRestController
 * @see io.kapsules.ingestion.api.TriggerRestController
 */
@Service
public class IngestionListener
    implements Consumer<ServerlessProtos.Event> {

  private static final Logger LOG = LoggerFactory.getLogger(IngestionListener.class);

  private final KafkaTemplate<Long, ServerlessProtos.Event> kafkaTemplate;

  @Autowired
  public IngestionListener(KafkaTemplate<Long, ServerlessProtos.Event> kafkaTemplate) {
    this.kafkaTemplate = kafkaTemplate;
  }

  @KafkaListener(
      topics = "${kafka.listeners.ingestion}",
      containerFactory = "eventsListenerContainerFactory",
      groupId = KAFKA_GROUP_ID
  )
  @Override
  public void accept(ServerlessProtos.Event event) {
    LOG.debug("event=\"{}\"", event);

    switch (event.getEventType()) {
      case NEW_TRIGGER_EVENT:
        // TODO: create the trigger and post it to Cassandra
        break;
      case NEW_FUNCTION_EVENT:
        // TODO: create a new Function; save to Cassandra and publish to the build queue.
        break;
      default:
        LOG.error("Unexpected event of type {} on ingestion queue; sent to dead letter "
            + "queue. id = {}", event.getEventType(), event.getId());
        kafkaTemplate.send(DEAD_LETTER_QUEUE, event);
    }
  }
}

// Copyright (c) 2018 Kapsules.io.  All rights reserved.

package io.kapsules.ingestion;

import io.kapsules.ScanBasePackageMarker;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackageClasses = {ScanBasePackageMarker.class})
public class IngestionApplication {

  public static void main(String[] args) {
    SpringApplication.run(IngestionApplication.class, args);
  }
}

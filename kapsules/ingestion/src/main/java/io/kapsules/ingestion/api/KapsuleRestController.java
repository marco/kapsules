// Copyright (c) 2018 Kapsules.io.  All rights reserved.

package io.kapsules.ingestion.api;

import com.google.common.annotations.VisibleForTesting;
import com.google.protobuf.ByteString;
import io.kapsules.common.utils.ProtoUtils;
import io.kapsules.core.ServerlessProtos;
import io.kapsules.domain.Kapsule;
import io.kapsules.domain.VersionedNamespaceAndNameKey;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.cassandra.core.ReactiveCassandraTemplate;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.lang.Nullable;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import static io.kapsules.common.SharedConstants.BUILD_EVENT;
import static io.kapsules.common.utils.ProtoUtils.toJson;
import static org.slf4j.LoggerFactory.getLogger;
import static org.springframework.data.cassandra.core.query.Criteria.where;
import static org.springframework.data.cassandra.core.query.Query.query;

@RestController
@RequestMapping(value = "/{org}/{scope}/function", produces = MediaType.APPLICATION_JSON_VALUE)
public class KapsuleRestController {

  private static final Logger LOG = getLogger(KapsuleRestController.class);

  private final String domain;
  private final ReactiveCassandraTemplate template;
  private final KafkaTemplate<Long, ServerlessProtos.Event> producer;

  @Value("#{buildTopic}")
  String topic;

  @Autowired
  public KapsuleRestController(
      String domain,
      ReactiveCassandraTemplate template,
      KafkaTemplate<Long, ServerlessProtos.Event> producer
  ) {
    this.domain = domain;
    this.template = template;
    this.producer = producer;
  }


  @PostMapping(value = "/{name}", consumes = {MediaType.TEXT_PLAIN_VALUE})
  public Mono<ResponseEntity<?>> postFunction(@PathVariable String org,
                                              @PathVariable String scope,
                                              @PathVariable String name,
                                              @RequestParam String version,
                                              @RequestBody String sourceCode
  ) {
    ServerlessProtos.Namespace namespace = ProtoUtils.Namespace.of(domain, org, scope).build();
    Kapsule function = new Kapsule(VersionedNamespaceAndNameKey.create(namespace, name, version));
    function.setBody(sourceCode);
    // TODO: when Auth is enabled, add the username that posted the function
    //    function.setUser(authUser);

    return template.insert(function)
        .doOnSuccess(
            f -> sendBuildEvent(function)
        )
        .map(f -> ResponseEntity.created(
                URI.create(String.format("/%s/%s/function/%s", org, scope, name)))
            .body(toJson(f.toProto())));
  }

  @GetMapping("/{name}")
  public Mono<ResponseEntity<String>> getFunction(
      @PathVariable String org,
      @PathVariable String scope,
      @PathVariable String name,
      @Nullable @RequestParam String version
  ) {
    // TODO: implement mapping 'latest' version to the most recent version; then if not specified
    // here, use it when retrieving data.
    if (StringUtils.isEmpty(version)) {
      return Mono.just(ResponseEntity.badRequest().body("A version must be specified"));
    }
    VersionedNamespaceAndNameKey key = VersionedNamespaceAndNameKey.create(
        ProtoUtils.Namespace.of(domain, org, scope).build(),
        name,
        version
    );

    return template.selectOneById(key, Kapsule.class)
        .map(f -> ResponseEntity.ok(toJson(f.toProto())))
        .defaultIfEmpty(ResponseEntity.notFound().build());
  }

  @GetMapping(value = "/{name}/body", produces = {MediaType.TEXT_PLAIN_VALUE})
  public Mono<ResponseEntity<String>> getFunctionBody(
      @PathVariable String org,
      @PathVariable String scope,
      @PathVariable String name,
      @RequestParam String version
  ) {
    VersionedNamespaceAndNameKey key = VersionedNamespaceAndNameKey.create(
        ProtoUtils.Namespace.of(domain, org, scope).build(),
        name,
        version
    );

    return template.selectOneById(key, Kapsule.class)
        .map(Kapsule::getBody)
        .map(ResponseEntity::ok)
        .defaultIfEmpty(ResponseEntity.notFound().build());
  }

  @GetMapping("")
  public Mono<ResponseEntity<List<String>>> getAllFunctionsForOrgScope(
      @PathVariable String org,
      @PathVariable String scope
  ) {
    Flux<Kapsule> results = template.select(
        query(where("domain").is(domain))
          .and(where("org").is(org))
          .and(where("scope").is(scope))
        .withAllowFiltering(),
        Kapsule.class);

    return results.map(Kapsule::getTag)
        .collectList()
        .map(ResponseEntity::ok)
        .defaultIfEmpty(ResponseEntity.notFound().build());
  }

  // TODO: make async.
  private void sendBuildEvent(Kapsule kapsule) {
    ServerlessProtos.Event event = ProtoUtils.Event.of(
        kapsule.getPk().getNamespace(),
        kapsule.getTag(),
        BUILD_EVENT)
        .setSerializedAs(ServerlessProtos.Event.SerializedAs.TEXT)
        .setPayload(ByteString.copyFromUtf8(kapsule.getBody()))
        .setId(UUID.randomUUID().toString())
        .build();
    LOG.debug("Sending Event \n--\n{}\n--\nto topic: '{}'", event, topic);
    try {
      long offset = producer.send(topic, event).get().getRecordMetadata().offset();
      LOG.debug("Published event: '{}', Offset: {}", event.getEventType(), offset);
    } catch (InterruptedException | ExecutionException e) {
      // TODO: either send to DeadLetterQueue, or insert into a "retry queue" - this is a serious
      // failure, cannot just be logged away.
      LOG.error(String.format("Could not publish build event to topic %s: %s", topic, e
          .getLocalizedMessage()), e);
    }
  }
}

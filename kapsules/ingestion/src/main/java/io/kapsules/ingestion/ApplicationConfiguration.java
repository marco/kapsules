// Copyright (c) 2018 Kapsules.io.  All rights reserved.

package io.kapsules.ingestion;

import io.kapsules.common.config.BaseConfiguration;
import io.kapsules.common.utils.ProtoUtils;
import io.kapsules.core.ServerlessProtos;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationConfiguration extends BaseConfiguration {

  @Bean
  ServerlessProtos.Source self() {
    return ProtoUtils.Source.of(
        ProtoUtils.Namespace.of(domain(), "system", "server").build(),
        Constants.INGESTION_SERVER).build();
  }
}

// Copyright (c) 2018 Kapsules.io.  All rights reserved.

package io.kapsules.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.kapsules.core.ServerlessProtos;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import java.util.UUID;

/**
 * Allows searching triggers by name (scoped by
 * {@link io.kapsules.core.ServerlessProtos.Namespace}) and obtain their UUID; this in turn can
 * be used to retrieve the full {@link Trigger}.
 *
 * @author marco
 */
@Data
@AllArgsConstructor
@Table("triggers_by_name")
public class TriggerByName {

  @PrimaryKey
  NamespaceAndNameKey nameKey;

  @NonNull @Column("trigger_id")
  @JsonProperty("trigger_id")
  UUID triggerId;

  /**
   * Factory method to create a {@link TriggerByName} from its Protocol Buffer representation.
   *
   * @param trigger a PB for this trigger
   * @return a newly created object that can be saved in Cassandra
   */
  public static TriggerByName fromProto(ServerlessProtos.Trigger trigger) {
    NamespaceAndNameKey key = NamespaceAndNameKey.fromNamespace(
        trigger.getTriggerSource().getNamespace(), trigger.getName());
    return new TriggerByName(key, UUID.fromString(trigger.getId()));
  }
}

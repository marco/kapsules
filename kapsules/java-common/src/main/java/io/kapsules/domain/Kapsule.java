// Copyright (c) 2018 Kapsules.io.  All rights reserved.

package io.kapsules.domain;


import io.kapsules.common.SharedConstants;
import io.kapsules.core.ServerlessProtos;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import org.slf4j.Logger;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import java.util.Date;
import java.util.regex.Matcher;

import static org.slf4j.LoggerFactory.getLogger;

@Data
@EqualsAndHashCode(exclude = {"when"})
@Table("kapsules")
public class Kapsule {

  private static final Logger LOG = getLogger(Kapsule.class);

  @NonNull @PrimaryKey
  VersionedNamespaceAndNameKey pk;


  @Column
  Date when = new Date();

  @Column
  String user;

  @Column
  String body;

  public String getTag() {
    return String.format("%s/%s/%s/%s:%s",
        pk.getDomain(),
        pk.getOrg(),
        pk.getScope(),
        pk.getName(),
        pk.getVersion());
  }

  public static Kapsule fromProto(ServerlessProtos.Function function) {
    return parseTag(function.getTag());
  }

  public static Kapsule parseTag(String tag) {
    final Matcher m = SharedConstants.TAG_PATTERN.matcher(tag);

    if (m.matches()) {
      VersionedNamespaceAndNameKey pk = new VersionedNamespaceAndNameKey(
          m.group("domain"),
          m.group("org"),
          m.group("scope"),
          m.group("name"),
          m.group("version")
      );
      return new Kapsule(pk);
    } else {
      throw new IllegalArgumentException(tag + " is not a valid function tag");
    }
  }

  public ServerlessProtos.Function toProto() {
    return ServerlessProtos.Function.newBuilder()
        .setNs(pk.getNamespace())
        .setTag(getTag())
        .build();
  }
}

// Copyright (c) 2018 Kapsules.io.  All rights reserved.

package io.kapsules.domain;

import io.kapsules.common.utils.ProtoUtils;
import io.kapsules.core.ServerlessProtos;
import lombok.*;
import org.springframework.data.cassandra.core.cql.Ordering;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyClass;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;

@Data
@PrimaryKeyClass
public class NamespaceAndNameKey {

  @NonNull @PrimaryKeyColumn(ordinal = 0, type = PrimaryKeyType.PARTITIONED)
  private String domain;

  @NonNull @PrimaryKeyColumn(ordinal = 1, type = PrimaryKeyType.PARTITIONED)
  private String org;

  @NonNull @PrimaryKeyColumn(ordinal = 2, type = PrimaryKeyType.CLUSTERED,
      ordering = Ordering.ASCENDING)
  private String scope;

  @NonNull @PrimaryKeyColumn(ordinal = 3, type = PrimaryKeyType.CLUSTERED,
      ordering = Ordering.ASCENDING)
  private String name;


  /**
   * Convenience factory method to build a PK for the <pre>functions</pre> table, given a
   * {@link ServerlessProtos.Source Source Protobuf}; also used in
   * the <pre>triggers_by_source</pre> table.
   *
   * @param source the PB from which the org, scope and name will be extracted
   * @return a PK suitable for lookups in the <pre>functions</pre> table.
   */
  public static NamespaceAndNameKey fromSource(ServerlessProtos.Source source) {
    return fromNamespace(source.getNamespace(), source.getName());
  }

  public static NamespaceAndNameKey fromNamespace(ServerlessProtos.Namespace ns, String name) {
    return new NamespaceAndNameKey(ns.getDomain(), ns.getOrg(), ns.getScope(), name);
  }

  public ServerlessProtos.Namespace toNamespace() {
    return ProtoUtils.Namespace.of(domain, org, scope).build();
  }
}

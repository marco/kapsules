package io.kapsules.domain;

import io.kapsules.common.utils.ProtoUtils;
import io.kapsules.core.ServerlessProtos;
import lombok.Data;
import lombok.NonNull;
import org.springframework.data.cassandra.core.cql.Ordering;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyClass;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;

@Data
@PrimaryKeyClass
public class VersionedNamespaceAndNameKey  {

  @NonNull @PrimaryKeyColumn(ordinal = 0, type = PrimaryKeyType.PARTITIONED)
  private String domain;

  @NonNull @PrimaryKeyColumn(ordinal = 1, type = PrimaryKeyType.PARTITIONED)
  private String org;

  @NonNull @PrimaryKeyColumn(ordinal = 2, type = PrimaryKeyType.CLUSTERED,
      ordering = Ordering.ASCENDING)
  private String scope;

  @NonNull @PrimaryKeyColumn(ordinal = 3, type = PrimaryKeyType.CLUSTERED,
      ordering = Ordering.ASCENDING)
  private String name;

  @NonNull @PrimaryKeyColumn(ordinal = 4, type = PrimaryKeyType.CLUSTERED,
      ordering = Ordering.ASCENDING)
  private String version;

  // Factory methods

  public static VersionedNamespaceAndNameKey create(NamespaceAndNameKey ns, String version) {
    return new VersionedNamespaceAndNameKey(ns.getDomain(), ns.getOrg(), ns.getScope(),
        ns.getName(), version);
  }

  public static VersionedNamespaceAndNameKey create(
      ServerlessProtos.Namespace ns,
      String name,
      String version) {
    return create(NamespaceAndNameKey.fromNamespace(ns, name), version);
  }

  public static VersionedNamespaceAndNameKey createUnversioned(ServerlessProtos.Source source) {
    return create(NamespaceAndNameKey.fromSource(source), "");
  }

  public ServerlessProtos.Namespace getNamespace() {
    return ProtoUtils.Namespace.of(domain, org, scope).build();
  }
}

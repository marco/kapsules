// Copyright (c) 2018 Kapsules.io.  All rights reserved.

package io.kapsules.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.Maps;
import io.kapsules.core.ServerlessProtos;
import lombok.Data;
import lombok.NonNull;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import java.util.Collections;
import java.util.Map;
import java.util.UUID;

/**
 * This class encapsulates a minimalist view of Triggers, mapped by "Source": this is meant to be a
 * fast and efficient way to retrieve the state and function mapping of Triggers by the Scheduler
 * service, so only a subset of the information associated with a {@link Trigger} is retained here.
 *
 * <p>This object should only (or primarily) be used to determine the state of a trigger and its
 * unique ID, which can then be used to retrieve the full information about the trigger.
 *
 * <p>See also the "Join on Write" pattern of Cassandra data modeling.
 *
 * @author marco
 */
@Data
@Table("triggers_by_source")
public class TriggerBySource {

  /**
   * This table is indexed by the {@link io.kapsules.core.ServerlessProtos.Source} that would cause
   * this trigger to invoke the mapped function (assuming the {@literal eventType} matches).
   *
   * <p>So, both the {@link io.kapsules.core.ServerlessProtos.Namespace} and {@literal name} are
   * those of the source for the event, not for the Trigger (although, currently, the namespace is
   * the same).
   *
   * <p>The trigger's full details can be retrieved via the {@link Trigger} table, indexed by
   * this {@link #triggerId}.
   */
  @NonNull
  @PrimaryKey
  NamespaceAndNameKey source;

  @NonNull
  @Column("trigger_id")
  @JsonProperty("trigger_id")
  UUID triggerId;

  @Column("state")
  String state;

  @Column("function_map")
  Map<String, String> eventToFunctionMap = Maps.newHashMap();

  public Map<String, ServerlessProtos.Function> toFunctionMapping() {
    return eventToFunctionMap == null
        ? Collections.emptyMap() :
        Maps.toMap(eventToFunctionMap.keySet(),
            k -> {
              String tag = eventToFunctionMap.get(k);
              return ServerlessProtos.Function.newBuilder()
                  .setTag(tag)
                  .setNs(source.toNamespace())
                  .build();
            });
  }

  /**
   * Factory method to obtain a {@link TriggerBySource} from its Protocol Buffer representation.
   *
   * @param trigger the source Protobuf
   * @return a newly-create {@link TriggerBySource}
   */
  public static TriggerBySource fromProto(ServerlessProtos.Trigger trigger) {
    ServerlessProtos.Source source = trigger.getTriggerSource();

    TriggerBySource triggerBySource = new TriggerBySource(
        NamespaceAndNameKey.fromSource(source),
        UUID.fromString(trigger.getId())
    );
    triggerBySource.setState(trigger.getState().name());

    Map<String, String> eventToFunctionMap = Maps.newHashMapWithExpectedSize(trigger
        .getEventTriggerMappingCount());
    trigger.getEventTriggerMappingMap()
        .forEach((eventType, function) -> {
          eventToFunctionMap.put(eventType, function.getTag());
        });
    triggerBySource.setEventToFunctionMap(eventToFunctionMap);
    return triggerBySource;
  }
}

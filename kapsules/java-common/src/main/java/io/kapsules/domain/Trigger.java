// Copyright (c) 2018 Kapsules.io.  All rights reserved.

package io.kapsules.domain;

import io.kapsules.common.utils.ProtoUtils;
import io.kapsules.core.ServerlessProtos;
import lombok.Data;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;
import reactor.core.publisher.Mono;

import java.nio.ByteBuffer;
import java.util.UUID;

@Data
@Table("triggers")
public class Trigger {

  @PrimaryKey("trigger_id")
  UUID id;

  @Column
  ByteBuffer serializedPb;

  public static Trigger fromProto(ServerlessProtos.Trigger trigger) {
    Trigger newTrigger = new Trigger();
    newTrigger.setId(UUID.fromString(trigger.getId()));
    newTrigger.setSerializedPb(ByteBuffer.wrap(trigger.toByteArray()));

    return newTrigger;
  }

  public Mono<ServerlessProtos.Trigger> toProto() {
    return ProtoUtils.quietlyParse(ByteBuffer.wrap(serializedPb.array()),
        ServerlessProtos.Trigger.class);
  }
}

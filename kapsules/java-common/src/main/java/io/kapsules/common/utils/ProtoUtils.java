// Copyright (c) 2018 Kapsules.io.  All rights reserved.

package io.kapsules.common.utils;

import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Message;
import com.google.protobuf.util.JsonFormat;
import io.kapsules.common.SharedConstants;
import io.kapsules.core.ServerlessProtos;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import reactor.core.publisher.Mono;

import javax.validation.constraints.NotNull;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.nio.ByteBuffer;

/**
 * Collection of convenience method to build Protobuf objects via factory methods.
 */
public class ProtoUtils {

  private static final Logger LOG = LoggerFactory.getLogger(ProtoUtils.class);

  public static class Namespace {

    /**
     * Splits a string-encoded {@link ServerlessProtos.Namespace namespace} in its components.
     *
     * <p>E.g., {@literal example-eng-system} will be split into {@literal Namespace
     * ("example", "eng", "system"}
     *
     * <p>All of the elements <strong>must</strong> be present.
     *
     * <p>In order to comply with Kubernetes' <pre>namespace</pre>
     *
     * <a href="https://kubernetes.io/docs/concepts/overview/working-with-objects/namespaces/">
     * syntax</a> namespace components can only have alphanumeric characters and the
     * <strong>only valid separator is `-`</strong>.
     *
     * @param namespace the string to tokenize
     * @return the Protobuf object obtained by splitting the namespace string
     *
     * @throws IllegalArgumentException if {@literal namespace} cannot be split in exactly 3
     *     tokens
     */
    public static ServerlessProtos.Namespace.Builder of(String namespace) {

      if (SharedConstants.VALID_NAMESPACE_PATTERN.matcher(namespace).matches()) {
        String[] tokens = namespace.split("-");
        return of(tokens[0], tokens[1], tokens[2]);
      }
      throw new IllegalArgumentException(namespace + " is not a valid Namespace");
    }

    public static ServerlessProtos.Namespace.Builder of(String domain, String org, String scope) {
      return ServerlessProtos.Namespace.newBuilder()
          .setDomain(domain)
          .setOrg(org)
          .setScope(scope);
    }
  }

  public static class Event {
    public static ServerlessProtos.Event.Builder of(ServerlessProtos.Namespace namespace,
                                                    ServerlessProtos.Source source,
                                                    String eventType) {
      return ServerlessProtos.Event.newBuilder()
          .setNamespace(namespace)
          .setSource(source)
          .setEventType(eventType)
          .setTimestamp(System.currentTimeMillis());
    }

    public static ServerlessProtos.Event.Builder of(ServerlessProtos.Namespace namespace,
                                                    String sourceName,
                                                    String eventType) {
      return of(namespace, Source.of(namespace, sourceName).build(), eventType);
    }
  }

  public static class Source {
    public static ServerlessProtos.Source.Builder of(ServerlessProtos.Namespace namespace, String
        name) {
      return ServerlessProtos.Source.newBuilder()
          .setNamespace(namespace)
          .setName(name);
    }
  }

  public static String toJson(Message message) {
    final JsonFormat.Printer printer = JsonFormat.printer().includingDefaultValueFields();
    try {
      return printer.print(message);
    } catch (InvalidProtocolBufferException e) {
      LOG.error("Cannot convert message to JSON", e);
      return makeJsonError(String.format("Cannot convert Protobuf to JSON: %s", e
          .getLocalizedMessage()));
    }
  }

  public static ServerlessProtos.Error makeError(@NotNull String errorMsg, Integer code,
                                                 String detail) {
    ServerlessProtos.Error.Builder error = ServerlessProtos.Error.newBuilder()
        .setMessage(errorMsg);

    if (!StringUtils.isEmpty(detail)) {
      error.setDetail(detail);
    }
    if (code != null) {
      error.setCode(code);
    }

    return error.build();
  }

  public static ServerlessProtos.Error makeError(Throwable throwable) {
    String detail = null;
    if (throwable.getCause() != null) {
      detail = throwable.getCause().getMessage();
    }
    return makeError(throwable.getLocalizedMessage(), HttpStatus.BAD_REQUEST.value(), detail);
  }

  public static String makeJsonError(@NotNull String errorMsg) {
    return makeJsonError(errorMsg, null, null);
  }

  public static String makeJsonError(@NotNull String errorMsg, Integer code, String detail) {
    return toJson(makeError(errorMsg, code, detail));
  }

  public static boolean isBlank(ServerlessProtos.Namespace ns) {
    return (ns == null)
        || StringUtils.isEmpty(ns.getOrg()) && StringUtils.isEmpty(ns.getScope());
  }

  public static boolean isBlank(ServerlessProtos.Source source) {
    return (source == null)
        || isBlank(source.getNamespace()) && StringUtils.isEmpty(source.getName());
  }

  /**
   * Using some reflection magic, we try to parse the given byte buffer into a valid protocol buffer
   * message.
   *
   * <p>Tries to "be quiet" by swallowing all exceptions and just returning a
   * {@link Mono#empty()} if anything goes wrong; useful in functional code, when adding try/catch
   * blocks is tedious; error-prone; and, honestly, rather pointless anyway.
   *
   * @param bytes the serialized representation of the PB; <strong>must</strong> be of type
   *     {@link T}
   * @param clazz the class type of the PB, used to access the constructor via reflection
   * @param <T> the actual type of the Protobuf; must match the type that was serialized
   * @return if all goes well (and there is a lot that can possibly go wrong with this method) the
   *     deserialized representation of the Protobuf
   */
  public static <T extends Message> Mono<T> quietlyParse(ByteBuffer bytes, Class<T> clazz) {
    try {
      // Copied from the generated code in ServlessProtos.java
      com.google.protobuf.Parser<T> parser = new com.google.protobuf.AbstractParser<T>() {
        public T parsePartialFrom(
            CodedInputStream input,
            ExtensionRegistryLite extensionRegistry
        ) {
          try {
            Constructor<T> constructor = clazz.getDeclaredConstructor(CodedInputStream.class,
                ExtensionRegistryLite.class);
            // The constructor exists, but it is private: we make it visible here.
            constructor.setAccessible(true);
            return constructor.newInstance(input, extensionRegistry);
          } catch (NoSuchMethodException | IllegalAccessException | InstantiationException
              | InvocationTargetException e) {
            LOG.error("No valid constructor or no viable construction for Protobuf {}: {}",
                clazz.getCanonicalName(), e.getLocalizedMessage());
            return null;
          }
        }
      };
      T protobuf = parser.parseFrom(bytes);
      if (protobuf != null) {
        return Mono.just(protobuf);
      }
    } catch (InvalidProtocolBufferException ex) {
      // whatever, man...
      LOG.error("Could not parse Protocol Buffer from bytes: {}", ex.getLocalizedMessage());
    }
    return Mono.empty();
  }
}

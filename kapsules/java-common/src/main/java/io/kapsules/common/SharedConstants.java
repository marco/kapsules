// Copyright (c) 2018 Kapsules.io.  All rights reserved.

package io.kapsules.common;

import java.util.regex.Pattern;

public class SharedConstants {

  // MARK: Event Types for the Kafka queue.
  //
  public static final String BUILD_EVENT = "build";
  public static final String NEW_FUNCTION_EVENT = "new_function";
  public static final String NEW_TRIGGER_EVENT = "new_trigger";
  public static final String UPDATED_TRIGGER_EVENT = "updated_trigger";

  /**
   * This special queue is used to send messages in errors and is also used for Producers when a
   * more specific queue has not been configured: a monitoring server will be listening to
   * messages from this queue and will emit metrics and alerts as appropriate.
   */
  public static final String DEAD_LETTER_QUEUE = "serverless.dead_letter";

  // MARK: API Mapping
  //
  public static final String PAUSED_UNTIL = "paused_until";
  public static final String EVENT_MAPPING = "event_mapping";

  public static final String NAMESPACE_ELEMENT_REGEX = "[a-zA-Z0-9]+";

  public static final Pattern VALID_NAMESPACE_PATTERN = Pattern.compile(
      "(" + NAMESPACE_ELEMENT_REGEX + "-){2}" + NAMESPACE_ELEMENT_REGEX
  );
  public static final Pattern TAG_PATTERN = Pattern.compile(
      "(?<domain>\\S+)/(?<org>(\\w|-)+)/(?<scope>(\\w|-)+)/(?<name>\\w+):(?<version>\\S+)"
  );
}

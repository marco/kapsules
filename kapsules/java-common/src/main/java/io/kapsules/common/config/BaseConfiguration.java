// Copyright (c) 2018 Kapsules.io.  All rights reserved.

package io.kapsules.common.config;

import io.kapsules.ScanBasePackageMarker;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Base class for configuration classes, groups all common, shared annotations.
 */
@ComponentScan(basePackageClasses = ScanBasePackageMarker.class)
@EnableConfigurationProperties
@Configuration
public class BaseConfiguration {

  @Value("${domain}")
  String domain;

  @Bean
  public String domain() {
    return domain;
  }

}

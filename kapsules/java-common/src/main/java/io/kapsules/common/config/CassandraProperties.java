// Copyright (c) 2018 Kapsules.io.  All rights reserved.

package io.kapsules.common.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "cassandra")
@Data
public class CassandraProperties {

  private String contactpoints;
  private int port;
  private String keyspace;

  private int replicationFactor;

  private long timeoutMs;
}

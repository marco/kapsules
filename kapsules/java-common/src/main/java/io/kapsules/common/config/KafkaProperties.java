// Copyright (c) 2018 Kapsules.io.  All rights reserved.

package io.kapsules.common.config;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Kafka eventTopicMappings abstraction class.
 *
 * @author Marco Massenzio (mmassenzio@apple.com)
 */
@Component
@ConfigurationProperties(prefix = "kafka")
@Data
public class KafkaProperties {

  private static final Logger LOG = LoggerFactory.getLogger(KafkaProperties.class);

  /**
   * Number of max concurrent allowed threads when producing messages to Kafka.
   */
  private int concurrency;

  /**
   * See {@literal poll.ms} in the
   * <a href="https://kafka.apache .org/documentation/#newconsumerconfigs">Kafka Documentation</a>.
   */
  private int pollTimeout = 3000;

  /**
   * See {@literal enable.auto.commit} in the
   * <a href="https://kafka.apache.org/documentation/#newconsumerconfigs">Kafka Documentation</a>.
   */
  private boolean autoCommit = true;

  /**
   * See {@literal auto.commit.interval.ms} in the
   * <a href="https://kafka.apache .org/documentation/#newconsumerconfigs">Kafka Documentation</a>.
   */
  private int commitInterval = 5000;

  /**
   * EventTopicMapping names, by type of message.
   */
  private List<EventTopicMapping> producerTopics = Lists.newArrayList();

  /**
   * Listeners' topic names, by type of listener.
   */
  private Map<String, String> listeners = Maps.newHashMap();

  /**
   * Comma-separated list of Kafka Brokers to connect to, to bootstrap the client.
   */
  private String servers;

  private Map<String, String> topicForEventMap;


  @PostConstruct
  private void  initMapping() {
    topicForEventMap = Maps.newHashMapWithExpectedSize(producerTopics.size());
    for (EventTopicMapping eventTopicMapping : producerTopics) {
      topicForEventMap.put(eventTopicMapping.getEventType(), eventTopicMapping.getTopicName());
    }
  }

  public Optional<String> topicName(String eventType) {
    return Optional.ofNullable(topicForEventMap.get(eventType));
  }
}

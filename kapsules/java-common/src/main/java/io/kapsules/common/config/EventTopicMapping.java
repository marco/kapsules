// Copyright (c) 2018 Kapsules.io.  All rights reserved.

package io.kapsules.common.config;

import lombok.Data;

@Data
public class EventTopicMapping {
  String eventType;
  String topicName;
}

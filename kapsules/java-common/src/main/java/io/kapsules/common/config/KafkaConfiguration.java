// Copyright (c) 2018 Kapsules.io.  All rights reserved.

package io.kapsules.common.config;

import io.kapsules.common.utils.ProtobufEventConverter;
import io.kapsules.core.ServerlessProtos;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.LongDeserializer;
import org.apache.kafka.common.serialization.LongSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.config.KafkaListenerContainerFactory;
//CHECKSTYLE:OFF
import org.springframework.kafka.core.*;
import org.springframework.kafka.listener.ConcurrentMessageListenerContainer;
//CHECKSTYLE:ON

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static io.kapsules.common.SharedConstants.*;

/**
 * Kafka eventTopicMappings abstraction class.
 *
 * @author Marco Massenzio (mmassenzio@apple.com)
 */
@Configuration
@EnableKafka
public class KafkaConfiguration {

  private final KafkaProperties properties;

  @Autowired
  public KafkaConfiguration(KafkaProperties properties) {
    this.properties = properties;
  }

  @Bean
  public Set<String> topicsNames() {
    return properties.getTopicForEventMap().keySet();
  }

  @Bean
  public Map<String, String> topicNamesForEvents() {
    return properties.getTopicForEventMap();
  }

  @Bean
  public String kafkaServers() {
    return properties.getServers();
  }


  private Map<String, Object> producerProperties() {
    Map<String, Object> props = new HashMap<>();

    props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaServers());
    props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, LongSerializer.class);
    props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, ProtobufEventConverter.class);

    return props;
  }

  private Map<String, Object> consumerProperties() {
    Map<String, Object> props = new HashMap<>();

    props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaServers());
    props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, LongDeserializer.class);
    props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, ProtobufEventConverter.class);

    // Configures all producers to NOT commit the offset after EACH message, but to rely on
    // the client to commit automatically the offset every `commitInterval` msec.
    // This is the recommended configuration and preferable to manually committing the offset
    // (using, e.g., commitAsync()).
    props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, properties.isAutoCommit());
    props.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, properties.getCommitInterval());

    return props;
  }

  /**
   * Used for Producers when need to retrieve the topic name via SpEL ({@code "#{ingestionTopic}"}.
   *
   * <p>See <a href="https://bitbucket.org/marco/serverless/src/develop/docs/topics.md">topics.md</a>
   * for more details
   */
  @Bean
  public String ingestionTopic() throws Throwable {
    return properties.topicName(NEW_FUNCTION_EVENT).orElse(DEAD_LETTER_QUEUE);
  }

  /**
   * Used for Producers when need to retrieve the topic name via SpEL ({@code "#{buildTopic}"}.
   * See <a href="https://bitbucket.org/marco/serverless/src/develop/docs/topics.md">topics.md</a>
   * for more details
   */
  @Bean
  public String buildTopic() throws Throwable {
    return properties.topicName(BUILD_EVENT).orElse(DEAD_LETTER_QUEUE);
  }

  /**
   * Used for Producers when need to retrieve the topic name via SpEL ({@code "#{triggerTopic}"}.
   * See <a href="https://bitbucket.org/marco/serverless/src/develop/docs/topics.md">topics.md</a>
   * for more details
   */
  @Bean
  public String triggerTopic() throws Throwable {
    return properties.topicName(NEW_TRIGGER_EVENT).orElse(DEAD_LETTER_QUEUE);
  }

  @Bean
  public KafkaTemplate<Long, ServerlessProtos.Event> eventsProducer() {
    return new KafkaTemplate<>(eventsProducerFactory());
  }

  @Bean
  public ProducerFactory<Long, ServerlessProtos.Event> eventsProducerFactory() {
    return new DefaultKafkaProducerFactory<>(producerProperties());
  }

  @Bean
  KafkaListenerContainerFactory<ConcurrentMessageListenerContainer<Long,
      ServerlessProtos.Event>> eventsListenerContainerFactory() {

    ConcurrentKafkaListenerContainerFactory<Long, ServerlessProtos.Event> factory =
        new ConcurrentKafkaListenerContainerFactory<>();
    factory.setConcurrency(properties.getConcurrency());
    factory.getContainerProperties().setPollTimeout(properties.getPollTimeout());

    factory.setConsumerFactory(eventsConsumerFactory());

    return factory;
  }

  private ConsumerFactory<Long, ServerlessProtos.Event> eventsConsumerFactory() {
    return new DefaultKafkaConsumerFactory<>(consumerProperties());
  }
}

// Copyright (c) 2018 Kapsules.io.  All rights reserved.

package io.kapsules.common.utils;

import com.google.protobuf.InvalidProtocolBufferException;
import io.kapsules.core.ServerlessProtos;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class ProtobufEventConverter implements Serializer<ServerlessProtos.Event>,
    Deserializer<ServerlessProtos.Event> {

  private static final Logger LOG = LoggerFactory.getLogger(ProtobufEventConverter.class);

  @Override
  public ServerlessProtos.Event deserialize(String topic, byte[] data) {
    try {
      return ServerlessProtos.Event.parseFrom(data);
    } catch (InvalidProtocolBufferException e) {
      LOG.error("Could not parse bytes into Event Protobuf: {}", e.getLocalizedMessage());
    }
    return null;
  }

  @Override
  public void configure(Map<String, ?> configs, boolean isKey) {
    // This is a useful place where we can extract at properties configured at startup, which may
    // eventaully be used during each serialization/deserialization event.
  }

  @Override
  public byte[] serialize(String topic, ServerlessProtos.Event event) {
    return event.toByteArray();
  }

  @Override
  public void close() {
    // Nothing to do here.
    LOG.info("Closing Kafka ProtobufEventConverter class");
  }
}

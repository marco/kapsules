// Copyright (c) 2018 Kapsules.io.  All rights reserved.

package io.kapsules.common.config;


import io.kapsules.ScanBasePackageMarker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.cassandra.config.AbstractReactiveCassandraConfiguration;
import org.springframework.data.cassandra.config.SchemaAction;
import org.springframework.data.cassandra.core.cql.keyspace.CreateKeyspaceSpecification;
import org.springframework.data.cassandra.core.cql.keyspace.KeyspaceOption;
import org.springframework.data.cassandra.repository.config.EnableReactiveCassandraRepositories;

import java.time.Duration;
import java.util.Collections;
import java.util.List;

@Configuration
@EnableReactiveCassandraRepositories
public class CassandraConfiguration extends AbstractReactiveCassandraConfiguration {

  private final CassandraProperties cassandraProperties;

  @Autowired
  public CassandraConfiguration(CassandraProperties cassandraProperties) {
    this.cassandraProperties = cassandraProperties;
  }

  /*
   * Provide a contact point to the configuration.
   */
  public String getContactPoints() {
    return cassandraProperties.getContactpoints();
  }

  public int getPort() {
    return cassandraProperties.getPort();
  }


  @Override
  protected String getKeyspaceName() {
    return cassandraProperties.getKeyspace();
  }

  @Override
  protected List<CreateKeyspaceSpecification> getKeyspaceCreations() {
    CreateKeyspaceSpecification specification = CreateKeyspaceSpecification
        .createKeyspace(getKeyspaceName()).ifNotExists()
        .with(KeyspaceOption.DURABLE_WRITES, true)
        .withSimpleReplication();

    return Collections.singletonList(specification);
  }

  // The following configures Spring Cassandra to scan the packages for @Table annotations and
  // create the tables, if they don't already exist.

  @Override
  public String[] getEntityBasePackages() {
    return new String[] {ScanBasePackageMarker.class.getPackage().getName() + ".domain"};
  }

  @Override
  public SchemaAction getSchemaAction() {
    return SchemaAction.CREATE_IF_NOT_EXISTS;
  }

  @Bean
  public Duration timeout() {
    return Duration.ofMillis(cassandraProperties.getTimeoutMs());
  }
}


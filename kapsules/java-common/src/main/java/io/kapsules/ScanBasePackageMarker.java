// Copyright (c) 2018 Kapsules.io.  All rights reserved.

package io.kapsules;

import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Marker class for the {@link SpringBootApplication#scanBasePackageClasses() scanBasePackages}
 * annotation.
 *
 * <p>All Spring-annotated classes in this package and below will be auto-scanned.</p>
 */
public class ScanBasePackageMarker {
}

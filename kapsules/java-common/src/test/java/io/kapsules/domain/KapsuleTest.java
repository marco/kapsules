// Copyright (c) 2018 Kapsules.io.  All rights reserved.

package io.kapsules.domain;

import io.kapsules.common.BaseSpringIntegrationTest;
import io.kapsules.common.utils.ProtoUtils;
import io.kapsules.core.ServerlessProtos;
import org.cassandraunit.spring.CassandraDataSet;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.cassandra.core.ReactiveCassandraTemplate;

import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

@CassandraDataSet(value = {"kapsule-base.cql", "kapsule-test.cql"})
public class KapsuleTest extends BaseSpringIntegrationTest {

  @Autowired
  ReactiveCassandraTemplate template;

  @Test
  public void canGetFunctionDataWithCql() throws Exception {
    List<String> result = template.select("select body from kapsules WHERE "
        + "domain='testing' AND org='mktg' AND scope='revenues' "
        + "AND name='ad_billing' AND version='1.0'", String.class)
        .collectList().block();
    assertThat(result, notNullValue());
    assertThat(result.size(), is(1));
    assertThat(result.get(0), is("10 PRINT \"HELLO, MARCO\""));
  }

  @Test
  public void canGetFunctionData() throws Exception {
    ServerlessProtos.Namespace ns = ProtoUtils.Namespace.of("testing-mktg-campaigns").build();
    VersionedNamespaceAndNameKey key = VersionedNamespaceAndNameKey.create(
            ns, "ad_summary", "1.1"
    );

    Kapsule result = template.selectOneById(key, Kapsule.class).block();
    assertThat(result, notNullValue());
    assertThat(result.getBody(), is("IF(IA.EQ.0 .OR. IB.EQ.0 .OR. IC.EQ.0) STOP 1"));
  }

  @Test
  public void testProtos() {
    ServerlessProtos.Function function =
        ServerlessProtos.Function.newBuilder()
            .setTag("domain.com/eng/test/test_func:3456")
            .build();

    assertEquals("domain.com/eng/test/test_func:3456", function.getTag());
  }

  @Test
  public void testEventFactoryMethod() {
    ServerlessProtos.Namespace ns = ProtoUtils.Namespace.of("wonderful.org", "marketing",
        "ad-spend").build();
    ServerlessProtos.Event evt = ProtoUtils.Event.of(ns,
        ProtoUtils.Source.of(ns, "test-nameKey").build(),
        "test").build();

    assertThat(evt.getNamespace(), is(ns));
    assertThat(evt.getEventType(), is("test"));
    assertThat(evt.getSource().getName(), is("test-nameKey"));
  }
}

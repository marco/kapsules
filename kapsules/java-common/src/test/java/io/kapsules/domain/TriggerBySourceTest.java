// Copyright (c) 2018 Kapsules.io.  All rights reserved.

package io.kapsules.domain;

import io.kapsules.common.utils.ProtoUtils;
import io.kapsules.core.ServerlessProtos;
import org.junit.Test;

import java.util.UUID;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class TriggerBySourceTest {

  @Test
  public void fromProto() {
    UUID triggerId = UUID.randomUUID();

    ServerlessProtos.Namespace ns = ProtoUtils.Namespace.of("kapsules.io", "eng", "testing")
        .build();

    ServerlessProtos.Function function = ServerlessProtos.Function.newBuilder()
        .setTag("kapsules.io/dev/respond:2.0.3")
        .setNs(ns)
        .build();

    ServerlessProtos.Trigger pb = ServerlessProtos.Trigger.newBuilder()
        .setId(triggerId.toString())
        .setState(ServerlessProtos.State.PAUSED)
        // 01/19/2020 10:20:55
        .setPausedUntil(1579458055000L)
        .setTriggerSource(ProtoUtils.Source.of(ns, "ping"))
        .putEventTriggerMapping("ping", function)
        .build();

    TriggerBySource ts = TriggerBySource.fromProto(pb);
    NamespaceAndNameKey pk = new NamespaceAndNameKey("kapsules.io", "eng", "testing", "ping");

    assertThat(ts.getSource(), is(pk));
    assertThat(ts.getTriggerId(), is(triggerId));
    assertThat(ts.getState(), equalTo("PAUSED"));
    assertTrue(ts.getEventToFunctionMap().containsKey("ping"));
    assertThat(ts.getEventToFunctionMap().get("ping"), is(function.getTag()));
  }
}

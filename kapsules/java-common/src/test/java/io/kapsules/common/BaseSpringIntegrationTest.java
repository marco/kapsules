// Copyright (c) 2018 Kapsules.io.  All rights reserved.

package io.kapsules.common;

import io.kapsules.common.config.BaseConfiguration;
import io.kapsules.common.config.CassandraConfiguration;
import org.cassandraunit.spring.CassandraUnitTestExecutionListener;
import org.cassandraunit.spring.EmbeddedCassandra;
import org.cassandraunit.utils.EmbeddedCassandraServerHelper;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.test.rule.KafkaEmbedded;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;


/**
 * This class acts as the base for all integration test classes and should be extended by those.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(
    classes = {
        BaseConfiguration.class,
        CassandraConfiguration.class
    })
@EmbeddedCassandra
@TestExecutionListeners(value = { CassandraUnitTestExecutionListener.class },
    mergeMode = TestExecutionListeners.MergeMode.MERGE_WITH_DEFAULTS)
@ActiveProfiles("test")
public abstract class BaseSpringIntegrationTest {

  public static final String BUILD_TOPIC = "serverless.eventTopicMappings.build-test";
  public static final String INGEST_TOPIC = "serverless.eventTopicMappings.ingest-test";

  @ClassRule
  public static KafkaEmbedded kafka = new KafkaEmbedded(1, true,
      BUILD_TOPIC, INGEST_TOPIC
  );

  @BeforeClass
  public static void init() throws Exception {
    EmbeddedCassandraServerHelper.startEmbeddedCassandra();
  }

  @AfterClass
  public static void tearDown() {
  }
}

// Copyright (c) 2018 Kapsules.io.  All rights reserved.

package io.kapsules.common.utils;

import io.kapsules.core.ServerlessProtos;
import org.junit.Assert;
import org.junit.Test;

import java.nio.ByteBuffer;

import static io.kapsules.common.utils.ProtoUtils.quietlyParse;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class ProtoUtilsTest {

  @Test
  public void testNamespaceOf() {
    assertThat(ProtoUtils.Namespace.of("test-this-sport").getDomain(), is("test"));
    assertThat(ProtoUtils.Namespace.of("test-this-sport").getOrg(), is("this"));
    assertThat(ProtoUtils.Namespace.of("test-this-sport").getScope(), is("sport"));
  }

  @Test(expected = IllegalArgumentException.class)
  public void testIllegalNamespaceOf() {
    ProtoUtils.Namespace.of("-").build();
  }

  @Test(expected = IllegalArgumentException.class)
  public void testIllegalNamespaceOf2() {
    ProtoUtils.Namespace.of("test-;").build();
  }

  @Test(expected = IllegalArgumentException.class)
  public void testIllegalNamespaceOf3() {
    ProtoUtils.Namespace.of("").build();
  }

  @Test(expected = IllegalArgumentException.class)
  public void testIllegalNamespaceOf4() {
    ProtoUtils.Namespace.of("a space test-this-sport;").build();
  }

  @Test
  public void testQuietlyParse() {
    ServerlessProtos.Trigger trigger = ServerlessProtos.Trigger.newBuilder()
        .setId("someid")
        .setName("a name")
        .build();

    assertThat(quietlyParse(ByteBuffer.wrap(trigger.toByteArray()), ServerlessProtos.Trigger.class)
        .blockOptional()
        .orElseThrow(AssertionError::new), is(trigger));
  }
}

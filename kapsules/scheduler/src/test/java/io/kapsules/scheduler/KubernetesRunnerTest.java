package io.kapsules.scheduler;

import io.kapsules.common.utils.ProtoUtils;
import io.kapsules.core.ServerlessProtos;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class KubernetesRunnerTest {

  private KubernetesRunner runner = new KubernetesRunner("5000", null);

  @Test
  public void podNameFromFunctionTag() {
    String image = "registry:5000/dev-testing/mktg/campaigns/rev_reporting:1.0.3";
    ServerlessProtos.Event event = ServerlessProtos.Event.newBuilder()
        .setId("3c46ce70-b767-4861-aa40-8d6a8c0cc62c")
        .build();

    assertThat(runner.podNameFromFunctionTag(image, event), is("kapsule-rev_reporting-0cc62c"));

    // Function tags should **always** be versioned
  }
}

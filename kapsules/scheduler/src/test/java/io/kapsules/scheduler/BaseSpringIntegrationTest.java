// Copyright (c) 2018 Kapsules.io.  All rights reserved.

package io.kapsules.scheduler;

import io.kapsules.common.config.BaseConfiguration;
import io.kapsules.common.config.CassandraConfiguration;
import io.kapsules.common.config.KafkaConfiguration;
import io.kapsules.common.utils.ProtobufEventConverter;
import io.kapsules.core.ServerlessProtos;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.cassandraunit.spring.CassandraUnitTestExecutionListener;
import org.cassandraunit.spring.EmbeddedCassandra;
import org.cassandraunit.utils.EmbeddedCassandraServerHelper;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.KafkaMessageListenerContainer;
import org.springframework.kafka.listener.config.ContainerProperties;
import org.springframework.kafka.test.rule.KafkaEmbedded;
import org.springframework.kafka.test.utils.ContainerTestUtils;
import org.springframework.kafka.test.utils.KafkaTestUtils;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;



/**
 * This class acts as the base for all integration test classes and should be extended by those.
 *
 * <p><strong>This same class can be found in several different projects</strong> with slight
 * variations in name and/or set of annotations: this is due to:
 * <ul>
 * <li>Different project have slightly different configurations;</li>
 * <li>As it belongs to the `test` module, it will not be exported in the JAR;</li>
 * <li>Moving it to the `main` module would complicate imports (as the test packages from
 * Spring Boot are not available during the `compile` gradle phase).</li>
 * </ul>
 *
 * <p>The situation is not ideal, but it appears to be better than the alternatives.
 *
 * @author marco@alertavert.com
 */
@RunWith(SpringRunner.class)
@SpringBootTest(
    classes = {
        BaseConfiguration.class,
        ApplicationConfiguration.class,
        CassandraConfiguration.class,
        KafkaConfiguration.class,
        TestConfiguration.class
    },
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@EmbeddedCassandra
@TestExecutionListeners(value = {CassandraUnitTestExecutionListener.class},
    mergeMode = TestExecutionListeners.MergeMode.MERGE_WITH_DEFAULTS)
@ActiveProfiles("test")
@DirtiesContext
public abstract class BaseSpringIntegrationTest {
  public static final String ACTIVATION_TOPIC = "serverless.test-executions";
  public static final String SCHEDULE_TOPIC = "serverless.test-schedule";

  public static final String TRIGGERS_DIR = "/tmp/triggers";

  protected KafkaMessageListenerContainer<String, ServerlessProtos.Event> container;
  protected BlockingQueue<ServerlessProtos.Event> events;

  @ClassRule
  public static KafkaEmbedded kafka = new KafkaEmbedded(1, true, 1,
      ACTIVATION_TOPIC, SCHEDULE_TOPIC
  );

  /**
   * Class initialization.
   *
   * @throws Exception if it can't initialize CassandraUnit
   */
  @BeforeClass
  public static void init() throws Exception {
    System.setProperty("cassandra.triggers_dir", TRIGGERS_DIR);
    Path tmp = Paths.get(TRIGGERS_DIR);
    if (!tmp.toFile().exists()) {
      Files.createDirectories(tmp);
    }
    EmbeddedCassandraServerHelper.startEmbeddedCassandra();
  }

  protected void initializeEmbeddedKafka(String topic) {
    // set up the Kafka consumer properties
    Map<String, Object> consumerProperties =
        KafkaTestUtils.consumerProps("sender", "false", kafka);
    consumerProperties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
        ProtobufEventConverter.class);

    // create a Kafka consumer factory
    DefaultKafkaConsumerFactory<String, ServerlessProtos.Event> consumerFactory =
        new DefaultKafkaConsumerFactory<>(consumerProperties);

    // set the topic that needs to be consumed
    ContainerProperties containerProperties = new ContainerProperties(topic);

    // create a Kafka MessageListenerContainer
    container = new KafkaMessageListenerContainer<>(consumerFactory, containerProperties);

    // create a thread safe queue to store the received message
    events = new LinkedBlockingQueue<>();
  }

  protected void startEmbeddedKafka() throws Exception {
    // start the container and underlying message listener
    container.start();

    // wait until the container has the required number of assigned partitions
    ContainerTestUtils.waitForAssignment(container, kafka.getPartitionsPerTopic());
  }
}

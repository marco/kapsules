// Copyright (c) 2018 Kapsules.io.  All rights reserved.

package io.kapsules.scheduler.api;

import ai.grakn.redismock.RedisServer;
import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import io.kapsules.core.ServerlessProtos;
import io.kapsules.scheduler.BaseSpringIntegrationTest;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.cassandraunit.spring.CassandraDataSet;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.kafka.listener.MessageListener;
import redis.clients.jedis.Jedis;

import java.net.URI;
import java.util.Collections;
import java.util.UUID;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@CassandraDataSet(value = {"sched-triggers.cql"})
public class EventRestControllerTest extends BaseSpringIntegrationTest {
  @Rule
  public Timeout timeout = new Timeout(15, SECONDS);

  @Autowired
  TestRestTemplate restTemplate;

  @Autowired
  String domain;

  @Autowired
  Jedis redisClient;

  @Value("${kafka.listeners.activations}")
  String topic;

  @Value("${redis.port}")
  Integer redisPort;

  RedisServer redisServer;

  @Before
  public void initialize() throws Exception {
    initializeEmbeddedKafka(topic);

    // setup a Kafka message listener
    container.setupMessageListener(new MessageListener<String, ServerlessProtos.Event>() {
      @Override
      public void onMessage(ConsumerRecord<String, ServerlessProtos.Event> record) {
        events.add(record.value());
      }
    });
    startEmbeddedKafka();

    redisServer = RedisServer.newRedisServer(redisPort);
    redisServer.start();
  }

  @After
  public void stop() {
    // stop the container
    container.stop();
    redisServer.stop();
  }

  @Test
  public void postEvent() throws InterruptedException, InvalidProtocolBufferException {
    URI location = restTemplate.postForLocation(
        "/foo/bar/event/test2/source/a-name",
        Collections.EMPTY_MAP);

    assertThat(location, notNullValue());
    assertTrue("Was instead: " + location.getPath(),
        location.getPath().startsWith("/foo/bar/event/"));
    String uuid = location.getPath().substring(location.getPath().lastIndexOf('/') + 1);

    // Check that event was published to Kafka; if not, this will eventually time out.
    ServerlessProtos.Event evt = events.take();

    // Verify that the last segment of the URI is a valid UUID and matches the event published.
    assertThat(UUID.fromString(uuid), notNullValue());
    assertThat(uuid, equalTo(evt.getId()));

    // We verify here that the event that was posted on Kafka is the same that was stored in Redis.
    String serEvt = redisClient.get(uuid);
    assertThat(serEvt, notNullValue());
    ServerlessProtos.Event event =
        ServerlessProtos.Event.parseFrom(serEvt.getBytes());
    assertEquals(event, evt);
  }
}

// Copyright (c) 2018 Kapsules.io.  All rights reserved.

package io.kapsules.scheduler;

import ai.grakn.redismock.RedisServer;
import io.kapsules.common.utils.ProtoUtils;
import io.kapsules.core.ServerlessProtos;
import io.kapsules.domain.Kapsule;
import io.kapsules.domain.VersionedNamespaceAndNameKey;
import org.cassandraunit.spring.CassandraDataSet;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.listener.MessageListener;

import java.io.IOException;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;

@CassandraDataSet(value = {"kapsule-base.cql", "sched-triggers.cql"})
public class FunctionActivationsListenerTest extends BaseSpringIntegrationTest {

  @Rule
  public Timeout timeout = new Timeout(15, SECONDS);

  @Autowired
  private ServerlessProtos.Source self;

  @Autowired
  private KafkaTemplate<Long, ServerlessProtos.Event> kafkaTemplate;

  @Autowired
  private FunctionRunner mockRunner;

  @Autowired
  String domain;

  @Value("${redis.port}")
  Integer redisPort;

  RedisServer redisServer;

  @Before
  public void initialize() throws Exception {
    initializeEmbeddedKafka(ACTIVATION_TOPIC);

    // setup a Kafka message listener
    container.setupMessageListener((MessageListener<String, ServerlessProtos.Event>)
        record -> events.add(record.value()));

    startEmbeddedKafka();

    redisServer = RedisServer.newRedisServer(redisPort);
    redisServer.start();
  }

  @After
  public void stop() {
    container.stop();
    redisServer.stop();
    reset(mockRunner);
  }

  @Test
  public void eventTriggersExecution() throws IOException, InterruptedException {
    ServerlessProtos.Source buildSource = ProtoUtils.Source.of(
        ProtoUtils.Namespace.of("testing-foo-bar").build(),
        "a-name"
    ).build();

    ServerlessProtos.Event activation = ServerlessProtos.Event.newBuilder()
        .setEventType("test1")
        .setSource(buildSource)
        .build();
    kafkaTemplate.send(ACTIVATION_TOPIC, activation);

    Mockito.verify(mockRunner, Mockito.timeout(250)).execute(new Kapsule(
        new VersionedNamespaceAndNameKey(domain, "foo", "bar", "test", "1.0")
    ), activation);
  }

  @Test
  public void acceptDoesNotTriggerNoMatch() throws IOException, InterruptedException {
    ServerlessProtos.Source buildSource = ProtoUtils.Source.of(
        ProtoUtils.Namespace.of("test-foo-bar").build(),
        "a-name"
    ).build();

    ServerlessProtos.Event activation = ServerlessProtos.Event.newBuilder()
        .setEventType("fake")
        .setSource(buildSource)
        .build();
    kafkaTemplate.send(ACTIVATION_TOPIC, activation);

    // Give time to the Kafka message to be delivered.
    while (events.size() < 1) {
      MILLISECONDS.sleep(100);
    }
    // ... but this did not trigger execution.
    Mockito.verify(mockRunner, never()).execute(any(), any());
  }

  @Test
  public void acceptDoesNotTriggerDisabled() throws IOException, InterruptedException {
    ServerlessProtos.Source buildSource = ProtoUtils.Source.of(
        ProtoUtils.Namespace.of("testing-foo-paused").build(),
        "mock"
    ).build();

    ServerlessProtos.Event activation = ServerlessProtos.Event.newBuilder()
        .setEventType("test2")
        .setSource(buildSource)
        .build();
    kafkaTemplate.send(ACTIVATION_TOPIC, activation);

    // Give time to the Kafka message to be delivered.
    while (events.size() < 1) {
      MILLISECONDS.sleep(100);
    }

    // ... but this did not trigger execution.
    Mockito.verify(mockRunner, never()).execute(any(), any());
  }
}

// Copyright (c) 2018 Kapsules.io.  All rights reserved.

package io.kapsules.scheduler;

import com.google.common.annotations.VisibleForTesting;
import io.kapsules.core.ServerlessProtos;
import io.kapsules.domain.Kapsule;
import io.kubernetes.client.ApiException;
import io.kubernetes.client.apis.CoreV1Api;
//CHECKSTYLE:OFF
import io.kubernetes.client.models.*;
import org.springframework.beans.factory.annotation.Autowired;
import redis.clients.jedis.Jedis;
//CHECKSTYLE:ON

import java.util.Random;

/**
 * Executes the function invoking the K8s API Controller.
 */
public class KubernetesRunner extends AbstractBaseRunner {

  @Autowired
  String redisUri;

  public KubernetesRunner(String registryServerPort, Jedis redisClient) {
    super(registryServerPort, redisClient);
    LOG.info("Starting Runner for Kubernetes, with registry: {}", registryUri);
  }

  @VisibleForTesting
  String podNameFromFunctionTag(String tag, ServerlessProtos.Event event) {


    int startPos = tag.lastIndexOf("/") + 1;
    int lastPos = tag.lastIndexOf(":");
    if (lastPos == -1) {
      lastPos = tag.length();
    }
    String functionName = tag.substring(startPos, lastPos);
    String suffix = event.getId().substring(event.getId().length() - 6);

    LOG.debug("Composing Pod Name from function name: '{}', and Event ID suffix: '{}' "
        + "(tag was: {})", functionName, suffix, tag);
    return String.format("kapsule-%s-%s", functionName, suffix);
  }

  @Override
  public void execute(Kapsule function, ServerlessProtos.Event event) {
    CoreV1Api api = new CoreV1Api();

    String imageTag = String.format("%s/%s", registryUri, function.getTag());
    String podName = podNameFromFunctionTag(imageTag, event);
    LOG.info("Executing [{}] as Pod [{}]", imageTag, podName);

    // 1. Describe the Pod's metadata
    V1ObjectMeta metadata = new V1ObjectMeta()
        .name(podName)
        .putLabelsItem("kind", "kapsule")
        .putLabelsItem("launched-by", KubernetesRunner.class.getName());

    // 2. Write the Pod spec:
    V1PodSpec spec = new V1PodSpec()
        .addContainersItem(
            new V1Container()
                .image(imageTag)
                .name(podName)
                .addEnvItem(new V1EnvVar()
                    .name("EVENT_ID")
                    .value(event.getId())
                )
                .addEnvItem(new V1EnvVar()
                    .name("REDIS_URL")
                    .value(redisUri)
                )
        );
    spec.setRestartPolicy("Never");

    // 3. Write in Java what is essentially a YAML file.
    V1Pod podDesc = new V1Pod()
        .apiVersion("v1")
        .kind("Pod")
        .metadata(metadata)
        .spec(spec);

    try {
      // TODO: use a separate namespace for functions - possibly use the function's Namespace
      V1Pod pod = api.createNamespacedPod("default", podDesc, "pretty");

      // FIXME: this should be done in a separate, non-blocking thread.
      LOG.info("Waiting for POD {} to run", podName);
      Thread.sleep(2_000);
      pod = api.readNamespacedPodStatus(podName, "default", "pretty");
      V1PodStatus status = pod.getStatus();
      LOG.info("Pod: {}; Status: {}", podName, status.getPhase());

    } catch (ApiException e) {
      // TODO: See #157180005 (As a User I would like to know those Pods that failed and why)
      LOG.error("Failed to execute Pod {} [Function: {}]. Reason: {}",
          podName, function.getTag(), e.getLocalizedMessage());
    } catch (InterruptedException e) {
      // TODO: I never really know WTF to do with this exception, other than shrugging my shoulders.
    }
  }
}

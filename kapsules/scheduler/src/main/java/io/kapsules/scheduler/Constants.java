// Copyright (c) 2018 Kapsules.io.  All rights reserved.

package io.kapsules.scheduler;

public interface Constants {

  public static final String KAFKA_GROUP_ID = "scheduler_group";
}

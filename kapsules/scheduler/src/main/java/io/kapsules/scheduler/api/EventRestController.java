// Copyright (c) 2018 Kapsules.io.  All rights reserved.

package io.kapsules.scheduler.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.protobuf.ByteString;
import io.kapsules.common.utils.ProtoUtils;
import io.kapsules.core.ServerlessProtos;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;
import redis.clients.jedis.Jedis;

import javax.annotation.Nullable;
import java.net.URI;
import java.nio.ByteBuffer;
import java.util.Map;
import java.util.UUID;

import static io.kapsules.common.utils.ProtoUtils.toJson;

@RestController
@RequestMapping("/{org}/{scope}/event")
public class EventRestController {

  private static final ObjectMapper MAPPER = new ObjectMapper();
  private static final Logger LOG = LoggerFactory.getLogger(EventRestController.class);

  private final String domain;
  private final Jedis redisClient;
  private final KafkaTemplate<Long, ServerlessProtos.Event> producer;

  @Value("${kafka.listeners.activations}")
  String activationTopic;

  @Autowired
  public EventRestController(
      String domain,
      Jedis redisClient,
      KafkaTemplate<Long, ServerlessProtos.Event> producer
  ) {
    this.domain = domain;
    this.redisClient = redisClient;
    this.producer = producer;
    this.producer.setDefaultTopic(activationTopic);
  }

  @PostMapping(value = "/{eventType}/source/{name}", consumes = {MediaType.APPLICATION_JSON_VALUE})
  public ResponseEntity<?> postEvent(@PathVariable String org,
                                     @PathVariable String scope,
                                     @PathVariable String eventType,
                                     @PathVariable String name,
                                     @Nullable @RequestBody Map<String, ?> body) {
    UUID evtId = UUID.randomUUID();
    ServerlessProtos.Namespace ns = ProtoUtils.Namespace.of(domain, org, scope).build();
    ServerlessProtos.Event.Builder eventBuilder = ServerlessProtos.Event.newBuilder()
        .setId(evtId.toString())
        .setNamespace(ns)
        .setSource(ProtoUtils.Source.of(ns, name))
        .setEventType(eventType);

    try {
      if (body != null && body.containsKey("payload")) {
        String payload = MAPPER.writeValueAsString(body.get("payload"));
        if (!StringUtils.isEmpty(payload)) {
          eventBuilder.setSerializedAs(ServerlessProtos.Event.SerializedAs.JSON)
              .setPayload(ByteString.copyFromUtf8(payload));
          LOG.debug("Sending event payload: {}", payload);
        }
      }
    } catch (JsonProcessingException ex) {
      String errMsg = String.format("Invalid JSON Payload (%s): %s", ex.getLocation(),
          ex.getMessage());
      LOG.error(errMsg, ex);
      return ResponseEntity.badRequest().body(errMsg);
    }

    ServerlessProtos.Event event = eventBuilder.build();
    producer.send(activationTopic, event);

    System.out.println(String.format(">>>>> /%s/%s/event/%s", org, scope, evtId));
    return ResponseEntity.created(
        URI.create(String.format("/%s/%s/event/%s", org, scope, evtId)))
        .build();
  }

  @GetMapping("/{uuid}")
  public Mono<ResponseEntity<String>> getEventById(@PathVariable UUID uuid) {
    return Mono.just(redisClient.get(uuid.toString()))
        .flatMap(serialized -> ProtoUtils.quietlyParse(ByteBuffer.wrap(serialized.getBytes()),
            ServerlessProtos.Event.class))
        .map(evt -> ResponseEntity.ok(toJson(evt)))
        .defaultIfEmpty(ResponseEntity.notFound().build());
  }
}

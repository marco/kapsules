// Copyright (c) 2018 Kapsules.io.  All rights reserved.

package io.kapsules.scheduler.config;

public enum FunctionRunnerType {
  DOCKER, KUBERNETES;
}

// Copyright (c) 2018 Kapsules.io.  All rights reserved.

package io.kapsules.scheduler;

import io.kapsules.core.ServerlessProtos;
import io.kapsules.domain.Kapsule;

/**
 * Implementations of this interface will interact with the underlying runtime system and will
 * invoke execution of the serverless function.
 *
 * @author marco@alertavert.com
 */
public interface FunctionRunner {

  /**
   * Invocation of the function <strong>must</strong> be in a separate process, or background thread
   * (less desirable): callers of this method will assume that it is non-blocking, and it
   * <em>may</em> lead to thread/memory exhaustion if instead it is.
   *
   * @param function the serverless function to execute
   * @param event the {@link io.kapsules.core.ServerlessProtos.Event event} that triggered the
   *     execution.
   */
  void execute(Kapsule function, ServerlessProtos.Event event);
}

// Copyright (c) 2018 Kapsules.io.  All rights reserved.

package io.kapsules.scheduler.messaging;

import io.kapsules.core.ServerlessProtos.Event;
import io.kapsules.core.ServerlessProtos.Source;
import io.kapsules.domain.NamespaceAndNameKey;
import io.kapsules.scheduler.Constants;
import io.kapsules.scheduler.FunctionRunner;
import io.kapsules.scheduler.data.TriggerDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import redis.clients.jedis.Jedis;

import java.util.function.Consumer;

import static io.kapsules.common.utils.ProtoUtils.isBlank;


@Service
public class FunctionActivationsListener implements Consumer<Event> {

  private static final Logger LOG = LoggerFactory.getLogger(FunctionActivationsListener.class);

  private final FunctionRunner runner;
  private final TriggerDao dao;
  private final Jedis redisClient;

  /**
   * Listener for the function activation topic.
   *
   * @param runner the execution runtime for the function (e.g., a Kubernetes launcher)
   * @param dao the DAO for the {@link io.kapsules.domain.Trigger trigger}
   * @param redisClient a connector to a Redis cluster, to retrieve the {@link Event} that
   *     triggered the function
   */
  @Autowired
  public FunctionActivationsListener(FunctionRunner runner, TriggerDao dao, Jedis redisClient) {
    this.runner = runner;
    this.dao = dao;
    this.redisClient = redisClient;
  }

  @KafkaListener(
      topics = "${kafka.listeners.activations}",
      containerFactory = "eventsListenerContainerFactory",
      groupId = Constants.KAFKA_GROUP_ID
  )
  @Override
  public void accept(Event event) {
    LOG.debug("Function Activations Listener, received event=\"{}\"", event);

    Source source = event.getSource();
    String eventType = event.getEventType();
    if (isBlank(source) || StringUtils.isEmpty(eventType)) {
      LOG.error("No source found in activation event, or no eventType: discarding event {}",
          event);
      // TODO: publish the Event to a Dead letter Queue.
      return;
    }

    // TODO: explore using byte[] arguments here.
    // TODO: set a TTL for the event, so the Redis cache size doesn't grow uncontrollably.
    redisClient.set(event.getId(), new String(event.toByteArray()));

    NamespaceAndNameKey pk = NamespaceAndNameKey.fromSource(source);
    LOG.debug("Looking up function for event Source: {}", source);
    dao.getFunctionForEvent(pk, eventType)
        .ifPresent(function -> {
          LOG.info("Executing {}", function.getTag());
          runner.execute(function, event);
        });
  }
}

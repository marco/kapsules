// Copyright (c) 2018 Kapsules.io.  All rights reserved.

package io.kapsules.scheduler.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "containers-registry")
@Data
public class RegistryProperties {
  String uri;
  String server;
  int port;
}

// Copyright (c) 2018 Kapsules.io.  All rights reserved.

package io.kapsules.scheduler;

import io.kapsules.ScanBasePackageMarker;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackageClasses = {ScanBasePackageMarker.class})
public class SchedulerApplication {

  public static void main(String[] args) {
    SpringApplication.run(SchedulerApplication.class, args);
  }
}

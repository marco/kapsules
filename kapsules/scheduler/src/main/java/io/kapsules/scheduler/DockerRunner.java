// Copyright (c) 2018 Kapsules.io.  All rights reserved.

package io.kapsules.scheduler;

import io.kapsules.core.ServerlessProtos;
import io.kapsules.domain.Kapsule;
import redis.clients.jedis.Jedis;

import java.io.IOException;
import java.io.InputStream;

public class DockerRunner extends AbstractBaseRunner {

  /**
   * Creates a Docker runner, which points to the registry whose URI (in the form {@literal
   * host:port}) is passed.
   *
   * @param registryServerPort the host:port URI of the registry to use for images
   * @param redisClient a configured Redis client to use when publishing the event for the
   *     function to retrieve
   */
  public DockerRunner(String registryServerPort, Jedis redisClient) {
    super(registryServerPort, redisClient);
    LOG.info("Starting Scheduler service Runner for Docker, with registry: {}", registryUri);
  }

  @Override
  public void execute(Kapsule function, ServerlessProtos.Event event) {

    redisClient.set(event.getId(), new String(event.toByteArray()));

    String dockerImageTag = String.format("%s/%s", registryUri, function.getTag());
    LOG.info("docker run {}", dockerImageTag);
    try {
      // TODO: use a dynamically-build List<String> for the `command` instead.
      // TODO: make the network name an optional and configurable argument.
      ProcessBuilder pb = new ProcessBuilder("docker", "run", "--rm",
          "--env", String.format("EVENT_ID=%s", event.getId()),
          "--network", "serverless_serverless-net",
          dockerImageTag)
          .redirectErrorStream(true);
      Process p = pb.start();

      // TODO: do NOT wait for container execution; this must be handed over to a ThreadPoolExecutor
      int exitCode = p.waitFor();
      LOG.debug("docker exit code: {}", exitCode);

      byte[] buffer = new byte[20000];
      InputStream stdout = p.getInputStream();
      stdout.read(buffer);
      LOG.debug(new String(buffer));

    } catch (InterruptedException | IOException ex) {
      LOG.error("Could not execute docker run: {}", ex.getLocalizedMessage());
    }
  }
}

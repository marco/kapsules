// Copyright (c) 2018 Kapsules.io.  All rights reserved.

package io.kapsules.scheduler;

import org.slf4j.Logger;
import redis.clients.jedis.Jedis;

import static org.slf4j.LoggerFactory.getLogger;

public abstract class AbstractBaseRunner implements FunctionRunner {
  protected static final Logger LOG = getLogger(DockerRunner.class);
  protected final String registryUri;
  protected Jedis redisClient;

  public AbstractBaseRunner(String registryServerPort, Jedis redisClient) {
    registryUri = registryServerPort;
    this.redisClient = redisClient;
  }
}

// Copyright (c) 2018 Kapsules.io.  All rights reserved.

package io.kapsules.scheduler;

import io.kapsules.common.utils.ProtoUtils;
import io.kapsules.core.ServerlessProtos;
import io.kapsules.scheduler.config.FunctionRunnerType;
import io.kapsules.scheduler.config.RedisProperties;
import io.kapsules.scheduler.config.RegistryProperties;
import io.kubernetes.client.ApiClient;
import io.kubernetes.client.util.Config;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;
import redis.clients.jedis.Jedis;

import java.io.IOException;

@Configuration
public class ApplicationConfiguration {

  private final RedisProperties redis;
  private final RegistryProperties registry;

  @Value("${domain}")
  String domain;

  @Value("${runner}")
  FunctionRunnerType runnerType;

  @Autowired
  public ApplicationConfiguration(RegistryProperties registryProperties,
                                  RedisProperties redisProperties) {
    this.redis = redisProperties;
    this.registry = registryProperties;
  }

  @Bean
  public String domain() {
    return domain;
  }

  @Bean
  ServerlessProtos.Source self() {
    return ProtoUtils.Source.of(
        ProtoUtils.Namespace.of(domain, "system", "server").build(),
        "scheduler")
        .build();
  }

  @Bean
  FunctionRunner runner() throws IOException {
    String registryUri = StringUtils.isEmpty(registry.getUri())
        ? String.format("%s:%d", registry.getServer(), registry.getPort()) :
        registry.getUri();

    switch (runnerType) {
      case DOCKER:
        return new DockerRunner(registryUri, redisClient());

      case KUBERNETES:
        // TODO: confirm multi-threading behavior.
        // This uses the configuration provided in the YAML descriptor
        // pointed at by the $KUBECONFIG env var.
        ApiClient client = Config.defaultClient();
        io.kubernetes.client.Configuration.setDefaultApiClient(client);
        return new KubernetesRunner(registryUri, redisClient());

      default:
        throw new UnsupportedOperationException(
            "We only support DOCKER or KUBERNETES runners. Unsupported: " + runnerType);
    }
  }

  @Bean
  Jedis redisClient() {
    return new Jedis(redis.getHost(), redis.getPort());
  }

  @Bean
  String redisUri() {
    return String.format("%s:%d", redis.getHost(), redis.getPort());
  }
}

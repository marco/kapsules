// Copyright (c) 2018 Kapsules.io.  All rights reserved.

package io.kapsules.scheduler.data;

import io.kapsules.core.ServerlessProtos;
import io.kapsules.domain.Kapsule;
import io.kapsules.domain.NamespaceAndNameKey;
import io.kapsules.domain.TriggerBySource;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.cassandra.core.CassandraTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Optional;

import static org.slf4j.LoggerFactory.getLogger;

@Service
public class TriggerDao {
  private static final Logger LOG = getLogger(TriggerDao.class);

  private final CassandraTemplate cassandraTemplate;
  private final String domain;

  @Autowired
  public TriggerDao(CassandraTemplate cassandraTemplate, String domain) {
    this.cassandraTemplate = cassandraTemplate;
    this.domain = domain;
  }

  public Optional<Kapsule> getFunctionForEvent(
      NamespaceAndNameKey primaryKey,
      String eventType
  ) {
    TriggerBySource trigger = cassandraTemplate.selectOneById(primaryKey, TriggerBySource.class);
    if (trigger == null) {
      // TODO: is this a real warning situation? or the normal course of events, as it were?
      LOG.warn("No triggers found for source {}", primaryKey);
      return Optional.empty();
    }
    LOG.info("Found trigger for activation: trigger = {}", trigger);

    // A `null` or empty state is taken to indicate an ENABLED trigger (default state).
    if (StringUtils.isEmpty(trigger.getState())
        || ServerlessProtos.State.valueOf(trigger.getState()) == ServerlessProtos.State.ENABLED) {
      LOG.info("Found enabled trigger for event: {}", trigger.getTriggerId());
      LOG.debug("Mapping function to event type: {}", eventType);

      if (trigger.getEventToFunctionMap() == null
          || !trigger.getEventToFunctionMap().containsKey(eventType)) {
        LOG.debug("No mapping or no function mapped to event type = '{}'", eventType);
        return Optional.empty();
      }

      String functionTag = trigger.getEventToFunctionMap().get(eventType);
      if (functionTag != null) {
        LOG.info("Found function tag: {}", functionTag);
        // TODO: validate tag matches existing function
        // TODO: cache triggerId -> functionTag in Redis
        return Optional.of(Kapsule.parseTag(functionTag));
      }
    }

    LOG.debug("Trigger disabled; ignoring event = '{}'", eventType);
    return Optional.empty();
  }
}

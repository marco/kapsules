# Kapsules - On-Premise Serverless

Project   |  Kapsules
--------- | -----------------------
Version   |   0.6.1
Created   |   2018-01-13
Updated   |   2019-04-14
Author    |   M. Massenzio (marco@alertavert.com)

#### Copyright Kapsules.io (c) 2018. All rights reserved.

[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/ambv/black)

---

# Goal

The main objective is to create a self-contained, API-driven system for the execution
of independent snippets of code ("Code Capsules") based on a set of pre-determined
"Triggers", upon the occurrence of "Events."

The system is internally based on a continuous even stream, which drives all sub-systems;
these do not directly communicate between themselves, instead the publish/produce Events
to the stream and act upon receiving them.

Users of the system will have not to concern themselves with the runtime execution
environment of their Capsules, but will interact with other systems via predefined
client libraries and utilities which will enable them to, among other things:

- generate Events themselves;
- coordinate execution of other Capsules ("Pipeline" model);
- connect to external systems, databases and/or other API-driven systems.

This system will manage all compute, storage and network resources (using Kubernetes as
the scheduling and resource-management infrastructure; this being, however, an implementation
detail).

A Permission mechanism will enable tight control of security access such that only
authorized Actors will be able to install Capsules; define Triggers; and generate
Events.

### Contributing

Contributions (in the form of Pull Requests) are very much welcome: please see the [CONTRIBUTING](CONTRIBUTING.md) doc for more information.


# Architecture

![System diagram](docs/images/arch.png)

# Build & Setup

1. Build Protocol Buffers

    See the [Protobuf section](#protocol_buffers) for more details; however, to get
    it up and running quickly, set the `PROTOC_HOME` to point to the installation
    directory of `protoc` __3.5.1__ and then execute:

        ./scripts/build_proto.sh

2. Run Tests

        ./scripts/run_tests

    If the tests don't pass, it's pretty futile to carry on: first, troubleshoot the tests.

3. Run the local registry, before the other services, and then build the service containers:

        docker-compose up -d registry
        ./scripts/build_containers.sh

4. Deploy the stack using Compose

        docker-compose up -d

5. Test connectivity

        curl -fs http://localhost:9051/actuator/health | python -m json.tool
        curl -fs http://localhost:9052/actuator/health | python -m json.tool


If you see containers (especially Cassandra) failing with `Exit code 137`, that's
usually an OOM issue and _may_ be fixed increasing the RAM allocated to the Docker VM.


# Install Docker registry

The registry is deployed on the `minikube` cluster (see `kubernetes/registry.yaml`) and will run off port `30500` (the IP address can be found using `minikube ip`).

The data is persisted in a Docker-mounted volume (it will survive Docker daemon restarts and computer restarts - but stopping the container directly will clear all the persisted data).

Before using it, the (local) docker client needs to be configured to accept unauthenticated HTTP requests:

```shell
$ minikube ip
192.168.1.100

$ sudo vim /etc/docker/daemon.json     

{
	"insecure-registries": [
		"192.168.1.100:30500"
	]
}

$ sudo systemctl restart docker
```

Verify that the server is up & running:

```shell
$ curl -fs http://$(minikube ip):30500/v2/_catalog | python -m json.tool
{
    "repositories": [
        "serverless/entry",
        "serverless/kafka"
    ]
}
```
(you will need to push a couple of repositories first, obviously).

A [Web UI](https://hub.docker.com/r/hyper/docker-registry-web/) is also available on port `30501`.

These are exposed externally via `NodePort` services and can be reached at the `$(minikube ip)` IP address (and port `30500` and `30501`, respectively).

See the [YAML spec](kubernetes/registry.yaml) for details.

### Portainer

Adds a UI on top of Docker; not strictly required now that we are using Minikube.
See [portainer.io](http://portainer.io) for more info.

# Kubernetes

We run a [`Minikube`](https://github.com/kubernetes/minikube) single-node cluster; see the [deployment documentation](docs/Kapsules-deploy.md) for details.

**NOTE**
> Using docker-for-Mac's embedded Kubernetes cluster may work too, but there are some networking differences that make it difficult to write "wrapper" scripts that work on both MacOS and Linux; using `Minikube` on both OSes makes it for a much simpler development experience.


## Check status

To verify K8s is running:

    kubectl version
    kubectl cluster-info
    kubectl get cs

(the last invocation `cs` stands for "Components' Statuses")


## K8s UI

From a terminal start the dashboard service:

    $ kubectl create -f https://raw.githubusercontent.com/kubernetes/dashboard/master/src/deploy/recommended/kubernetes-dashboard.yaml

then from another, start the proxy service:

    $ kubectl proxy &
    Starting to serve on 127.0.0.1:8001

The Dashboard UI will be available at [this URL](http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/).

__NOTE__ The legacy `http://localhost:8001/ui` __does not work any longer__.


# Supporting Infrastructure Services

See the [deployment documentation](docs/Kapsules-deploy.md) for a description
of how `Kafka`, `Cassandra` and `Redis` are deployed.


## Kafka

**TODO**
> Kafka is currently deployed outside of the K8s cluster; **must** be added to the stack there too.

The Python driver for Kafka requires `librd`; on Linux:

```bash
wget -qO - https://packages.confluent.io/deb/4.0/archive.key | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://packages.confluent.io/deb/4.0 stable main"
sudo apt-get update && sudo apt-get install librdkafka-dev python-dev
```

on MacOS:

    brew install librdkafka

**NOTE**
> The above command may work and still the `pip install` fail - if it does, this may (or may not) fix it:
>
>```
>    brew update
>    brew upgrade librdkafka
>```

Also, due the way the Broker responds to clients, the `kafka` hostname must be resolvable locally; add the following to `/etc/hosts`:

    127.0.0.1   kafka

This is an example of how to connect to Kafka from a Python script:

```python
In [1]: from confluent_kafka import Producer

In [5]: p = Producer({'bootstrap.servers': 'kafka'})

In [6]: p.produce('testtop', 'this is just a test'.encode('utf-8'))

In [7]: from confluent_kafka import Consumer

In [8]: c = Consumer({'bootstrap.servers': 'kafka', 'group.id': 'mygroup',
   ...:               'default.topic.config': {'auto.offset.reset': 'smallest'}})

In [9]: c.subscribe(['testtop'])

In [10]: msg = c.poll()

In [11]: msg.value().decode('utf-8')
Out[11]: 'this is just a test'
```


# Protocol Buffers

Download binaries from [the release page](https://github.com/google/protobuf/releases), and make sure the `PROTOC_HOME` var points to the top folder.

Building the sources (Python / Java) will be done by the `build_proto.sh` script.


## Linux installation

    export PROTOC_HOME="/opt/protoc-3.5.1"
    sudo mkdir ${PROTOC_HOME} && cd ${PROTOC_HOME}
    sudo wget https://github.com/google/protobuf/releases/download/v3.5.1/protoc-3.5.1-linux-x86_64.zip && \
        unzip protoc-3.5.1-linux-x86_64.zip
    sudo chmod -R a+rX ./
    cd $BASEDIR
    ./scripts/build_proto.sh


## Python

**NOTE**
> For the purposes of the project, all Python support is managed via [`pipenv`](https://pipenv.readthedocs.io/en/latest/); just run `pipenv install --dev` from within the `python` directory.

Install Python support with:

    pip install protobuf

Reference API [documentation](https://developers.google.com/protocol-buffers/docs/reference/python/).

# BitBucket pipelines

**NOTE**
> After trying to use Pipelines, due to cost/complexity, this has now been disabled; we may take this up again for integration tests, but it seems unlikely as the free tier is very restrictive.

**The instructions below do not currently apply**

Sometimes it is difficult to replicate locally a test failure that happens remotely during the `pipelines` execution.

A workaround is to create a "local copy" of the container that runs the test on BitBucket servers, and run them from here, or even execute them manually from inside the container.

These are the steps (from `$BASEDIR`):

    # Build the container locally - make sure the steps in
    # scripts/run_pipeline.sh reflect the contents of pipelines.yaml
    #
    $ docker build -f docker/Dockerfile.pipelines -t pipelines .

    # Run the tests and save the HTML reports somewhere where they
    # can be accessed.
    #
    $ docker run --rm --name pipelines \
        -v /tmp/serverless:/opt/serverless/ingestion/build pipelines

    # If all else fails, "enter" the container and run tests manually;
    # edit code or configurations; or just explore what's missing.
    #
    $ docker run --rm -it --name pipelines pipelines /bin/bash

# References

    - [Kubernetes Java client](https://github.com/kubernetes-client/java)
    - [K8s Java Client API docs](https://github.com/kubernetes-client/java/tree/master/kubernetes#documentation-for-api-endpoints)
    - [API Documentation](https://web.postman.co/collections/18351-f0bc55b8-dfd6-95d2-424e-4c01fe440d32?workspace=071b8ab4-1875-4f77-819c-9f6a2cab4961)

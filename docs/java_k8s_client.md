# Java Kubernetes Client

Project   |  Kapsules
--------- | -----------------------
Version   |   0.4.0
Created   |   2018-04-24
Updated   |   2018-04-24
Author    |   M. Massenzio (marco@alertavert.com)

#### Copyright Kapsules.io (c) 2018. All rights reserved.


# Creating a Pod

A `Pod` describes the pod: [V1Pod](https://github.com/kubernetes-client/java/blob/master/kubernetes/docs/V1Pod.md)

It has, among other attributes, a `spec` that describes the Pod, and probably maps what the YAML file states:

[V1PodSpec](https://github.com/kubernetes-client/java/blob/master/kubernetes/docs/V1PodSpec.md)

And [this](https://github.com/kubernetes-client/java/blob/master/kubernetes/docs/CoreV1Api.md#createNamespacedPod) is how you create a `Pod`:

    V1Pod result = apiInstance.createNamespacedPod(namespace, body, pretty);

# Authentication

This needs to be figured out:

```
// Configure API key authorization: BearerToken
ApiKeyAuth BearerToken = (ApiKeyAuth) defaultClient.getAuthentication("BearerToken");
BearerToken.setApiKey("YOUR API KEY");
// The following line to set a prefix for the API key, e.g. "Token" (defaults to null)
BearerToken.setApiKeyPrefix("Token");
```

This seems to use [the Bearer Token](https://kubernetes.io/docs/admin/authentication/#static-token-file) functionality; essentially, add

    --token-auth-file=/path/to/tokens.csv

and each line in the file is something like:

    token,user,uid,"group1,group2,group3"

The "correct" use seems to be the one for [Service Account Tokens](https://kubernetes.io/docs/admin/authentication/#service-account-tokens), which however appears to be more complex (albeit, most amenable to automation).

# Client

By default uses the `${HOME}/.kube/config` file, that should be something like:

```
apiVersion: v1
clusters:
- cluster:
    certificate-authority: /home/marco/workspaces/linux/k8s-coreos/data/certs/ca.pem
    server: https://172.17.8.101
  name: default-cluster
contexts:
- context:
    cluster: default-cluster
    user: default-admin
  name: default-system
current-context: default-system
kind: Config
preferences: {}
users:
- name: default-admin
  user:
    client-certificate: /home/marco/workspaces/linux/k8s-coreos/data/certs/admin.pem
    client-key: /home/marco/workspaces/linux/k8s-coreos/data/certs/admin-key.pem
````



# Kapsules - On-Premise Serverless

Project   |  Kapsules
--------- | -----------------------
Version   |   0.2.0
Created   |   2018-03-18
Updated   |   2018-03-18
Author    |   M. Massenzio (marco@alertavert.com)

#### Copyright Kapsules.io (c) 2018. All rights reserved.

---

# Competitive Analysis


# Open Source solutions


## BlueNimble

[Code](https://github.com/bluenimble/serverless) and [website](https://www.bluenimble.com/).


## Serverless

[Site](https://serverless.com/)


## Apache Whisk

[Main site](https://openwhisk.apache.org/about.html) and [GitHub repository]()


## Docker Lambda:

[GitHub repository](https://github.com/lambci/docker-lambda)


## Iron Functions from Iron.io

[GitHub repository](https://github.com/iron-io/lambda) and [site](http://iron.io)

## Spring Serverless

[Video about PCF](https://springoneplatform.io/sessions/serverless-spring)

## Cloud Native Computing Foundation

[CNCF Serverless Working Group](https://github.com/cncf/wg-serverless)

## Fission

[fission.io](https://fission.io/) and [GitHub](https://github.com/fission/fission)

## Kubeless

[Kubeless](http://kubeless.io/) and [GitHub](https://github.com/kubeless/kubeless)

## OpenFaaS

[Site](https://www.openfaas.com/) and [GitHub](https://github.com/openfaas/faas).

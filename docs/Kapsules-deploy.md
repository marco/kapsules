# Kapsules - Deployment to Kubernetes

Project   |  Kapsules
--------- | -----------------------
Version   |   0.6.
Created   |   2018-09-05
Updated   |   2018-09-29
Author    |   M. Massenzio (marco@alertavert.com)


# Summary of Endpoints

Endpoint        | Deployment      | URL
----            | ----            | ----
Cassandra       | StatefulSet     | tcp://cx-dev-0.cx-dev:9042,<br>tcp://cx-dev-1.cx-dev:9042, ...<br>tcp://dev-cluster:9042
Kafka           | Docker Compose  | tcp://broker-0.dev-kafka:9092,<br>tcp://broker-1.dev-kafka:9092, ...<br>tcp://dev-kafka:9092
Redis           | StatefulSet     | tcp://redis-cluster:6379
Scheduler       | Deployment      | http://scheduler/api/v1
Ingestion       | Deployment      | http://ingestion/api/v1
Builder         | Deployment      | http://builder
Gateway         | Deployment      | http://kapsules
Registry        | Pod (NodePort)  | http://registry:5000, http://`$(minikube ip)`:30500
Registry UI     | Pod (NodePort)  | http://registry, http://`$(minikube ip)`:30501


# Development - Minikube

In development, we use [`Minikube`](), and deploying the `Kapsules` system will require, at least, the use of

```bash
minikube start --memory 8192 --cpus 4 \
    --vm-driver virtualbox -p serverless \
    --insecure-registry registry
```

adding the following to Minikube's VM `/etc/hosts`

```
$ minikube -p serverless ssh
$ sudo vi /etc/hosts

127.0.0.1    registry
```

This can be done remotely using:

```shell
$ minikube -p serverless ssh -- "echo -e '\n127.0.0.1\tregistry\n' | sudo tee -a /etc/hosts >/dev/null"
```

**Note**
> `tee` is required to bypass sudo permission issues with `/etc/hosts`.
>
>See [this page](https://askubuntu.com/questions/322494/edit-text-file-with-sudo-via-ssh-and-script) for more details).


# Deploying Services

All Kubernetes descriptors (YAML) are in the `kubernetes` folder; they can all be deployed with:

```bash
CONFIGS=($(find kubernetes -name "*.yaml"))
for yaml in ${CONFIGS[*]}; do kubectl apply -f ${yaml}; done
```

# Utility Containers

There are a number of "utility" containers that are useful for debugging and testing during development:

- [`dnsutils`](https://hub.docker.com/r/massenz/dnsutils/)
- [`pyproto` (Python 3.x)](https://hub.docker.com/r/massenz/pyproto/)
- [`cqlsh` (Cassandra 3.10)](https://hub.docker.com/_/cassandra/)

These all run in the `utils` Pod, launched with:

    $ kubectl apply -f kubernetes/utils.yaml

and can be used like this:

    $ kubectl exec -ti utils -c dnsutils -- nslookup test-cluster

# Registry

Both the backend and the Web UI can be started with:

    $ kubectl apply -f kubernetes/registry.yaml

the endpoint is `tcp://registry:5000` while the Web UI is reachable at `http://registry` from other Pods.

**NOTE**
>This **will not** work when *launching Pods* using image names such as `registry:5000/my-image`, as the Docker daemon running on the Minikube node has no knowledge (nor access) to `kubedns`; we must use something like `registry:30500/my-image` and map that hostname to `localhost` (`127.0.0.1`, or wherever else is deployed, depending on the configuration).

To reach them from the development host, they are reachable via the `NodePort` service, respectively on port `30500` and `30501` on the Node's external IP (use `$(minikube ip)` to discover).

When deploying Pods in the cluster, with images from this Registry, the URL must be modified accordingly (using, e.g., `$(minikube ip):30500`) so that the Docker server running on the Node can find and route to the Pod (the `scripts/build_containers.sh` script does that automatically).


# Persistent Services (StatefulSets)

## Cassandra

The service is deployed with:

    $ kubectl apply -f kubernetes/stateful/dev-cluster.yaml

The `cx-dev` backing service is "headless," so it can't be used to connect to the Pods: we also have the `dev-cluster` service to balance load across nodes: the Cassandra nodes are thus reachable at the following URLs:

    tcp://cx-dev-0.cx-dev:9042, tcp://cx-dev-1.cx-dev:9042, ...
    tcp://dev-cluster:9042

in the `default.svc.cluster.local` search domain:

```shell
$ kubectl exec -ti  utils -c dnsutils -- nslookup dev-cluster

Server:   10.96.0.10
Address:  10.96.0.10#53

Name: dev-cluster.default.svc.cluster.local
Address: 10.111.152.19
```

We can run a "client" Pod and use it to connect to our Cassandra test cluster; this is an example piping in a CQL test file:

```shell
$ kubectl exec -ti utils -c cqlsh \
    -- cqlsh dev-cluster < ${CFGDIR}/user.cql
```

### Reaching individual nodes

K8s sets up SRV records for the nodes in a StatefulSet, using the node name and the
"headless" service name to resolve the FQDNs:

```shell
$ kubectl exec -ti utils -c dnsutils \
    -- dig SRV cx-dev.default.svc.cluster.local
...
;; ANSWER SECTION:
cx-dev.default.svc.cluster.local. 30 IN SRV 10 100 0 cx-dev-0.cx-dev.default.svc.cluster.local.

;; ADDITIONAL SECTION:
cx-dev-0.cx-dev.default.svc.cluster.local. 30 IN A 172.17.0.4
```

which in turn means we can use something like this in the Pod definition:

```yaml
    - name: CASSANDRA_SEEDS
      value: cx-dev-0.cx-dev
```

and the nodes in the cluster can find each other.


## Redis

The cluster is deployed via:

    $ kubectl apply -f kubernetes/stateful/redis-cluster.yaml

and the cluster is then reachable at `tcp://redis-cluster:6379`.

This can be used to store a list of "pending events:" those that have triggered a lambda, whose execution has not been completed yet.

To connect via Python:

```python

$ kubectl exec -ti utils -c py3 python

>>> import redis
>>> r = redis.StrictRedis(host='localhost', port=6379, db=0)

>>> r.set('foo', 'bar')
True

>>> r.get('foo')
b'bar'
```

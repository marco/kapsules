# Serverless (fission.io)

## Ideas

- auto-generation of function skeleton

- validator for function

- `live reload` (`--watch` watches the filesystem)

- startup time: maybe have a `--dry-run` option to get it started the first time just to "pre-warm" the container cache

- Record/Replay of Events

- Add the Kapsule `tag` to the Pod labels

### Idea about upgrading version:

Return in the 201 response a list of {version / trigger} mappings that are impacted by the function tag we just uploaded:

```json
     [
        {
            "1.0.1": {
                "trigger_id": "10256-abedf-....",
                "trigger_name": "Update revenues"
                "event_types": [
                    "update",
                    "delete",
                    ...
                ]
            }
        },
        ...
     ]
```

## Rollout

Keep a record of failures/successes of a given Function
    - % increas of traffic to canaries (e.g., in 10% increments)
    - threshold errors that stop deployment of new version
    - rollout speed (how fast we increase if below threshold)

## Monitoring

> TODO: look into `fluentd` to ship logs to ELK

Use Prometheus with Alertmanager to set alerts based on metrics
> TODO: look into `Graphana` for dashboards

> Tracing: `jaeger` for OpenTracing



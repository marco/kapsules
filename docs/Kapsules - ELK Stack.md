# Kapsules - ELK Stack

Project   |  Kapsules
--------- | -----------------------
Version   |   0.4.0
Created   |   2018-04-02
Updated   |   2018-04-02
Author    |   M. Massenzio (marco@alertavert.com)

#### Copyright Kapsules.io (c) 2018. All rights reserved.

---

## Architecture

![Architecture](images/serverless-elk.png)

All services are configure to log to files in the shared `Volume` (as defined in the `docker-compose.yml`) and the same volume is bind-mounted to the [Logstash](https://www.elastic.co/guide/en/logstash/current/docker-config.html) container.

[Elasticsearch](https://www.elastic.co/guide/en/elasticsearch/reference/6.2/docker.html) is also run in its own container, with a bind-mount persistent volume (so data is preserved across restarts) and reached (across the shared `serverless-network`) by the other components (Kibana and Logstash).


## Logstash

This runs inside the Compose stack; however, it can be run in isolation with:

    $ docker run --rm -it docker.elastic.co/logstash/logstash-oss:6.2.3

To load the index into Elasticsearch, we need to get around the limitation of running a process into a Compose stack, so we will `exec` inside the running container (which knows how to reach the `elasticsearch` one):

    $ docker exec elk.beat filebeat setup --template \
        -E output.logstash.enabled=false \
        -E 'output.elasticsearch.hosts=["elasticsearch:9200"]'

All Java servers use the `logback.xml` configuration to log to a file in the shared volume, configured in `docker-compose.xml` and mounted to `/var/log/serverless/`.

The `elk.beat` container runs a `filebeat` Logstash plugin to collect the logs and send them to the `elk.logstash` container.

To change the logging configuration (e.g., when running inside IDE or for development) and console logging is wanted:

    -Dlogging.config=classpath:logback-dev.xml

Also, make sure there is a `logback-test.xml` in the `main/test/java/resources` folder, for when tests are run.

## Elasticsearch

We are currently using `docker.elastic.co/elasticsearch/elasticsearch-oss:6.2.3`.

This may fail if there is not enough memory available for Memory Mapped Files (`mmap`): see [the documentation](https://www.elastic.co/guide/en/elasticsearch/reference/5.0/vm-max-map-count.html#vm-max-map-count) causing the container to exit with an error log such as:

    [1]: max virtual memory areas vm.max_map_count [65530] is too low, increase to at least [262144]

This can be fixed by running:

    sudo sysctl -w vm.max_map_count=262144

and, permanently, by editing `/etc/sysctl.conf`:

    vm.max_map_count=262144


## Kibana

Image: `docker.elastic.co/kibana/kibana-oss:6.2.3`
UI: [http://localhost:8081](http://localhost:8081/app/kibana#/home?_g=())

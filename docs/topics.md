# Kapsules - Kafka Topics

Project   |  Kapsules
--------- | -----------------------
Version   |   0.5.0
Created   |   2018-07-18
Updated   |   2018-07-18
Author    |   M. Massenzio (marco@alertavert.com)

#### Copyright Kapsules.io (c) 2018. All rights reserved.

---

# Overview

All topics' messages are [`Event` Protobuf](../proto/src/main/proto/serverless.proto) byte-serialized and (optionally) carrying a serialized `payload`; the type of the payload can be inferred by the `eventType`, while the serialization is defined in the `serializedAs` field (typically, either `TEXT` or `PROTOBUF`).

In the list below, both sender(s) and receiver(s) are __not__ exhaustive, as other systems (including user-defined functions) could be consuming (and, possibly, producing) to these topics.


# Topics

Topic               | `serverless.activations`
---                 | ---
Event Type          | custom
Payload             | N/A
Known sender(s)     | N/A
Known receiver(s)   | Scheduler Server


Topic               | `serverless.build`
---                 | ---
Event Type          | `build`
Payload             | Function body
Known sender(s)     | Ingestion Server
Known receiver(s)   | PyLambda Builder


Topic               | `serverless.ingest`
---                 | ---
Event Type          | `new_trigger`, `new_function`
Payload             | `Trigger` (Protobuf) or Function body (text)
Known sender(s)     |
Known receiver(s)   | Ingestion Listener



Topic               | `serverless.schedule`
---                 | ---
Event Type          |
Payload             |
Known sender(s)     |
Known receiver(s)   |


Topic               | `serverless.trigger`
---                 | ---
Event Type          | `new_trigger`, `updated_trigger`
Payload             | `Trigger` (Protobuf)
Known sender(s)     | Ingestion Server
Known receiver(s)   |

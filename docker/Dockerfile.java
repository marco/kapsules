# Copyright AlertAvert.com (c) 2018.  All rights reserved.
#
# Dockerfile for BitBucket pipelines.
#
# Used to run tests for all Java servers, this is the
# image that is referred to as `massenz/javaproto:8`.
#
# Versioning follows the JDK version used here as base.

FROM java:8
MAINTAINER marco@alertavert.com

ENV WORKDIR="/opt"
ENV PROTOC_HOME="${WORKDIR}/protoc"

RUN mkdir -p ${PROTOC_HOME} && cd ${PROTOC_HOME} && \
    wget https://github.com/google/protobuf/releases/download/v3.5.1/protoc-3.5.1-linux-x86_64.zip && \
    unzip protoc-3.5.1*.zip

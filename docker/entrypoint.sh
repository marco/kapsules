#!/usr/bin/env bash
#
# Copyright Kapsules.io (c) 2018. All rights reserved.
#
# Trivial entrypoint for Java servers' containers.
# Only necessary as Docker does not expand ${vars} in
# the ENTRYPOINT command.

set -eu

declare -r LOGGING="${LOGGING:-classpath:logback.xml}"
declare -r SERVER_PORT=${SERVER_PORT:-8080}
declare -r ADDITIONAL_CFG=${ADDITIONAL_CFG:-/etc/kapsules/kapsules.yaml}


START_ARGS="${START_ARGS:-} -Dserver.port=${SERVER_PORT} \
-Dlogging.config=${LOGGING} \
-Dspring.config.additional-location=${ADDITIONAL_CFG}"

cmd="java ${START_ARGS} -jar ${JARFILE}"

echo "$cmd"
eval $cmd
